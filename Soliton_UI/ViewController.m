//
//  ViewController.m
//  Soliton_UI
//
//  Created by Chris Wong on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "ViewController.h"
#import "PlaylistTableViewCell.h"
#import "CommentTableViewCell.h"
#import "VideoTableViewCell.h"
#import "OptionBtnTableViewCell.h"
#import "ChartTableViewCell.h"
#import "GTNSStringAdditions.h"
#import "MusicTypeCellView.h"
#import "SOLHeaderTableViewCell.h"
#import "RecommendPlaylistTableViewCell.h"
#import "GTUIViewAdditions.h"
#import "ExploreCellView.h"
#import "NewsTableViewCell.h"
#import "NotificationsTableViewCell.h"
#import "EditTableViewCell.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITableView *tableView;
//@property (strong, nonatomic) IBOutlet UIView *testView;



@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return [NotificationsTableViewCell requiredHeightWithTitle:@"123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc"];
////    return 200;
//}

//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"OptionBtnTableViewCell";
//
//    OptionBtnTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        NSArray* btnOffImgArray = [NSArray arrayWithObjects: @"general_btn_more_favourite_white", @"general_btn_more_download_white", @"general_btn_more_playlist_white", @"general_btn_more_album_white", nil];
//        NSArray* btnOnImgArray = [NSArray arrayWithObjects: @"general_btn_more_favourite_yellow", @"general_btn_more_download_yellow", @"general_btn_more_playlist_yellow", @"general_btn_more_album_yellow", nil];
//        cell = [[OptionBtnTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                      reuseIdentifier:CellIdentifier buttonOnImageArray:btnOnImgArray buttonOffImageArray:btnOffImgArray];
//    }
//    
//    // Configure the cell...
//    [cell setButtonOptionTableViewCellButtonDidTap:^(OptionBtnTableViewCell *cell, UIButton *button, NSInteger index) {
//        NSLog(@"index:%ld",(long)index);
//    }];
//    return cell;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *identifier = @"Cell";
    
    EditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [EditTableViewCell gt_viewFromNib];
    }

//    ---chart---
//    [cell configCellWithImgUrl:nil title:@"123123123123123123123123123123123123123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc" ranking:indexPath.row+1 indicatorType:ChartIndicatorTypeUp];
//    [cell setChartTableViewCellRightButtonDidTap:^(ChartTableViewCell *cell, UIButton *button) {
//                NSLog(@"right btn click");
//            }];
//
//    ---video---
//    [cell configCellWithImgUrl:nil title:@"123123123123123123123123123123123123123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc"];
//    
//    ---playlist---
//    if (indexPath.row%2==0) {
        [cell configCellWithImgUrl:[@"http://www.cats.org.uk/uploads/images/pages/photo_latest14.jpg" gt_toURL] title:@"123123123123123123123123123123123123123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc" isHQ:YES isDownload:YES];
//    }else{
//        [cell configCellWithImgUrl:nil title:@"123123123123123123123123123123123123123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc" isHQ:YES isDownload:YES];
//
//    }
//    [cell setEditMode:YES];
//    
//    ---News---
//    if (indexPath.row%2==0) {
//        [cell configCellWithImgUrl:nil title:@"123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc"];
//    }else{
//        [cell configCellWithImgUrl:[@"http://www.cats.org.uk/uploads/images/pages/photo_latest14.jpg" gt_toURL] title:@"123" content:@"abcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabc"];
//    }
    
    //header
//    [cell configCellWithTitle:@"testing" totalNum:10];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

@end
