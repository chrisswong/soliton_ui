//
//  MusicCardCellView.h
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicCardCellView : UIView

- (void) configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title;
- (void) setTextAlignment:(NSTextAlignment)textAlignment;
- (void) setTextCenterVertically;

@end
