//
//  ArtistCellView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "ArtistCellView.h"
#import "SOLCircleImageView.h"
#import "GTUIViewAdditions.h"

@interface ArtistCellView ()
@property (strong, nonatomic) IBOutlet SOLCircleImageView *profileView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLbl;
@end

@implementation ArtistCellView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        [self setSubView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setSubView];
    }
    return self;
}


- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"ArtistCellView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];
}


- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title subTitle:(NSString *)subTitle{
    [_profileView setImageWithImageUrl:imgUrl];
    _titleLbl.text = title;
    _subTitleLbl.text = subTitle;
}

@end
