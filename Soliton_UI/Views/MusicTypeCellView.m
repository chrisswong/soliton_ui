//
//  MusicTypeCellView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "MusicTypeCellView.h"

@implementation MusicTypeCellView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)layoutSubviews{
    [self setTextAlignment:NSTextAlignmentCenter];
    [self setTextCenterVertically];
}

@end
