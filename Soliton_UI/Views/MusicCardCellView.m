//
//  MusicCardCellView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "MusicCardCellView.h"
#import "SOLImageView.h"

@interface MusicCardCellView ()
@property (strong, nonatomic) IBOutlet SOLImageView *coverView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@end

@implementation MusicCardCellView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        [self setSubView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setSubView];
    }
    return self;
}

- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"MusicCardCellView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];
}


- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title{
    [_coverView setImageWithImageUrl:imgUrl];
    _titleLbl.text = title;
}


- (void) setTextAlignment:(NSTextAlignment)textAlignment{
    _titleLbl.textAlignment = textAlignment;
}


- (void) setTextCenterVertically{    
    [self addConstraint:
     [NSLayoutConstraint constraintWithItem:_titleLbl
                                  attribute:NSLayoutAttributeCenterY
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self
                                  attribute:NSLayoutAttributeCenterY
                                 multiplier:1
                                   constant:0]];

}



@end
