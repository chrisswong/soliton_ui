//
//  SOLTableViewCellWithImageNonAnimateView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 5/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLTableViewCellWithImageNonAnimateView.h"

@interface SOLTableViewCellWithImageNonAnimateView ()
@end

@implementation SOLTableViewCellWithImageNonAnimateView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self setSubView];
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self setSubView];
    
    return self;
}


- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"SOLTableViewCellWithImageNonAnimateView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];
    
    _circleIV.hidden = YES;
    
    __weak typeof(self) weakSelf = self;
    [_rightContentView setSolTableViewCellNonAnimateViewRightButtonDidTap:^(UIButton *button) {
        if (weakSelf.solTableViewCellWithImageNonAnimateViewRightButtonDidTap) {
            weakSelf.solTableViewCellWithImageNonAnimateViewRightButtonDidTap(button);
        }
    }];
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content isCircleImage:(BOOL)isCircleImage{
    if (isCircleImage) {
        [_circleIV setImageWithImageUrl:imgUrl];
        _circleIV.hidden = NO;
        _rectangleIV.hidden = YES;
    }else{
        [_rectangleIV setImageWithImageUrl:imgUrl];
        _circleIV.hidden = YES;
        _rectangleIV.hidden = NO;
    }
    [_rightContentView configCellWithTitle:title content:content];
    
}

- (void) setRectangleImageRatio:(CGFloat)ratio{
    [_rectangleIV removeConstraint:_squareIVAspectRatioConstraint];
    NSLayoutConstraint *constraint =[NSLayoutConstraint
                                     constraintWithItem:_rectangleIV
                                     attribute:NSLayoutAttributeHeight
                                     relatedBy:NSLayoutRelationEqual
                                     toItem:_rectangleIV
                                     attribute:NSLayoutAttributeWidth
                                     multiplier:ratio
                                     constant:0.0f];
    [_rectangleIV addConstraint:constraint];
}



@end
