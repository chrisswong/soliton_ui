//
//  SOLTableViewCellNonAnimateView.h
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOLTableViewCellNonAnimateView : UIView

- (void)configCellWithTitle:(NSString *)title content:(NSString *)content;
- (void)configCellWithTitle:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload;
- (void)setRightBtnImg:(UIImage *)img;
- (void)setRightBtnHidden:(BOOL)isHidden;
- (void)setTitleNumberOfLines:(NSInteger)num;
- (void)setContentNumberOfLines:(NSInteger)num;
- (void)setTitleFont:(UIFont *)titleFont;
- (void)setContentFont:(UIFont *)contentFont;
- (void)setTitleColor:(UIColor *)titleColor;
- (void)setContentColor:(UIColor *)contentColor;


@property (nonatomic, copy) void (^solTableViewCellNonAnimateViewRightButtonDidTap)(UIButton *sender);

@end
