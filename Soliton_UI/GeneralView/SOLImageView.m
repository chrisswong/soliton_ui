//
//  SOLImageView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLImageView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SOLImageView ()
@property (strong, nonatomic) IBOutlet UIImageView *contentIV;
@end

@implementation SOLImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        [self setSubView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setSubView];
    }
    return self;
}


- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"SOLImageView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];
}


- (void)setImageWithImageUrl:(NSURL *)imgUrl{
    [_contentIV sd_setImageWithURL:imgUrl];
}


@end
