//
//  SOLTableViewCellAnimateView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLTableViewCellAnimateView.h"
#import "CBAutoScrollLabel.h"
@interface SOLTableViewCellAnimateView ()
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *titleLbl;
@property (strong, nonatomic) IBOutlet CBAutoScrollLabel *contentLbl;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;
@property (strong, nonatomic) IBOutlet UIImageView *playingIV;
@end

@implementation SOLTableViewCellAnimateView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self setSubView];
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self setSubView];
    
    return self;
}


- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"SOLTableViewCellAnimateView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];
    
    _titleLbl.textColor = [UIColor whiteColor];
    _contentLbl.textColor = [UIColor whiteColor];
}


- (void)configCellWithTitle:(NSString *)title content:(NSString *)content{
    [self configCellWithTitle:title content:content isHQ:NO isDownload:NO];
}

- (void)configCellWithTitle:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload{
    NSTextAttachment *attachmentHQ = [[NSTextAttachment alloc] init];
    attachmentHQ.image = [UIImage imageNamed:@"general_icon_hq"];
    NSAttributedString *attachmentHQString = [NSAttributedString attributedStringWithAttachment:attachmentHQ];
    
    NSTextAttachment *attachmentDownload = [[NSTextAttachment alloc] init];
    attachmentDownload.image = [UIImage imageNamed:@"general_icon_downloaded"];
    NSAttributedString *attachmentDownloadString = [NSAttributedString attributedStringWithAttachment:attachmentDownload];
    
    NSMutableAttributedString *titleString= [[NSMutableAttributedString alloc] initWithString:title];
    NSAttributedString *spaceString= [[NSAttributedString alloc] initWithString:@" "];
    if (isHQ) {
        [titleString appendAttributedString:spaceString];
        [titleString appendAttributedString:attachmentHQString];
    }
    if (isDownload) {
        [titleString appendAttributedString:spaceString];
        [titleString appendAttributedString:attachmentDownloadString];
    }
    
    _titleLbl.attributedText = titleString;
    _contentLbl.text = content;
}

- (IBAction)rightBtnAction:(id)sender {
    if (self.solTableViewCellAnimateViewRightButtonDidTap) {
        self.solTableViewCellAnimateViewRightButtonDidTap(sender);
    }
}

- (void)setTitleColor:(UIColor *)titleColor contentColor:(UIColor *)contentColor{
    _titleLbl.textColor = titleColor;
    _contentLbl.textColor = contentColor;
}

- (void)setRightBtnImg:(UIImage *)img{
    [_rightBtn setImage:img forState:UIControlStateNormal];
}

- (void)setRightBtnHidden:(BOOL)isHidden{
    _rightBtn.hidden = isHidden;
}

- (void)setPlayingIVHidden:(BOOL)isHidden{
    _playingIV.hidden = isHidden;
}

- (void)setLabelScrollSpeed:(NSInteger)speed{
    _titleLbl.scrollSpeed = speed;
    _contentLbl.scrollSpeed = speed;
}



@end
