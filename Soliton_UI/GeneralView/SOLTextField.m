//
//  SOLTextField.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLTextField.h"

@implementation SOLTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [[UIColor whiteColor]CGColor];
    self.layer.borderWidth = 1.0f;
    
    self.backgroundColor = [UIColor clearColor];
    
    [self setPlaceholderColor:[UIColor grayColor]];
    
}


- (void) setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = [borderColor CGColor];
}

- (void) setTextFont:(UIFont *)textFont{
    self.font = textFont;
}

- (void) setPlaceholderFont:(UIFont *)placholderFont{
    if(self.placeholder){
        NSAttributedString *placeholderAttributedString = [[NSAttributedString alloc] initWithString:[self.placeholder uppercaseString] attributes:@{ NSFontAttributeName : placholderFont}];
        self.attributedPlaceholder = placeholderAttributedString;
    }
}

- (void) setPlaceholderColor:(UIColor *)placholderColor{
    if(self.placeholder){
        NSAttributedString *placeholderAttributedString = [[NSAttributedString alloc] initWithString:[self.placeholder uppercaseString] attributes:@{ NSForegroundColorAttributeName : placholderColor }];
        self.attributedPlaceholder = placeholderAttributedString;
    }
}


@end
