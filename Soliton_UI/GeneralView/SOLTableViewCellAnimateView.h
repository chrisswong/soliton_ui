//
//  SOLTableViewCellAnimateView.h
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOLTableViewCellAnimateView : UIView

- (void)configCellWithTitle:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload;
- (void)configCellWithTitle:(NSString *)title content:(NSString *)content;
- (void)setTitleColor:(UIColor *)titleColor contentColor:(UIColor *)contentColor;
- (void)setRightBtnImg:(UIImage *)img;
- (void)setRightBtnHidden:(BOOL)isHidden;
- (void)setPlayingIVHidden:(BOOL)isHidden;
- (void)setLabelScrollSpeed:(NSInteger)speed;

@property (nonatomic, copy) void (^solTableViewCellAnimateViewRightButtonDidTap)(UIButton *sender);

@end
