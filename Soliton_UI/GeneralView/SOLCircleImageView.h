//
//  SOLCircleImageView.h
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLImageView.h"

@interface SOLCircleImageView : SOLImageView

- (void) setBorderColor:(UIColor *)borderColor;

@end
