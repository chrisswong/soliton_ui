//
//  SOLCircleImageView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLCircleImageView.h"
#import "GTUIViewAdditions.h"

@implementation SOLCircleImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)layoutSubviews{
    [self gt_addCircleCornerRadius];
    
    self.layer.borderColor = [[UIColor redColor]CGColor];
    self.layer.borderWidth = 1.0f;
}

- (void) setBorderColor:(UIColor *)borderColor{
    self.layer.borderColor = [borderColor CGColor];
}

@end
