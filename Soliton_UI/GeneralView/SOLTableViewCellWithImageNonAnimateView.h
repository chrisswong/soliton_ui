//
//  SOLTableViewCellWithImageNonAnimateView.h
//  Soliton_UI
//
//  Created by Joyce Tam on 5/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLImageView.h"
#import "SOLCircleImageView.h"
#import "SOLTableViewCellNonAnimateView.h"

@interface SOLTableViewCellWithImageNonAnimateView : UIView

@property (strong, nonatomic) IBOutlet SOLImageView *rectangleIV;
@property (strong, nonatomic) IBOutlet SOLCircleImageView *circleIV;
@property (strong, nonatomic) IBOutlet SOLTableViewCellNonAnimateView *rightContentView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *squareIVAspectRatioConstraint;

- (void) configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content isCircleImage:(BOOL)isCircleImage;
- (void) setRectangleImageRatio:(CGFloat)ratio;

@property (nonatomic, copy) void (^solTableViewCellWithImageNonAnimateViewRightButtonDidTap)(UIButton *sender);

@end
