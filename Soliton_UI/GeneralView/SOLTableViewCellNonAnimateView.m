//
//  SOLTableViewCellNonAnimateView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 2/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLTableViewCellNonAnimateView.h"
#import "SOLMultiLineLabel.h"
#import "SOLImageView.h"
#import "SOLCircleImageView.h"

@interface SOLTableViewCellNonAnimateView ()
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet SOLMultiLineLabel *contentLbl;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightBtnWidthConstraint;
@end

@implementation SOLTableViewCellNonAnimateView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(!self){
        return nil;
    }
    
    [self setSubView];
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(!self){
        return nil;
    }
    
    [self setSubView];
    
    return self;
}


- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"SOLTableViewCellNonAnimateView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];

}


- (void)configCellWithTitle:(NSString *)title content:(NSString *)content{
    [self configCellWithTitle:title content:content isHQ:NO isDownload:NO];
}

- (void)configCellWithTitle:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload{
    NSTextAttachment *attachmentHQ = [[NSTextAttachment alloc] init];
    attachmentHQ.image = [UIImage imageNamed:@"general_icon_hq"];
    NSAttributedString *attachmentHQString = [NSAttributedString attributedStringWithAttachment:attachmentHQ];
    
    NSTextAttachment *attachmentDownload = [[NSTextAttachment alloc] init];
    attachmentDownload.image = [UIImage imageNamed:@"general_icon_downloaded"];
    NSAttributedString *attachmentDownloadString = [NSAttributedString attributedStringWithAttachment:attachmentDownload];
    
    NSMutableAttributedString *titleString= [[NSMutableAttributedString alloc] initWithString:title];
    NSAttributedString *spaceString= [[NSAttributedString alloc] initWithString:@" "];
    if (isHQ) {
        [titleString appendAttributedString:spaceString];
        [titleString appendAttributedString:attachmentHQString];
    }
    if (isDownload) {
        [titleString appendAttributedString:spaceString];
        [titleString appendAttributedString:attachmentDownloadString];
    }
    
    _titleLbl.attributedText = titleString;
    _contentLbl.text = content;
}


- (IBAction)rightBtnAction:(id)sender {
    if (self.solTableViewCellNonAnimateViewRightButtonDidTap) {
        self.solTableViewCellNonAnimateViewRightButtonDidTap(sender);
    }
}

- (void)setRightBtnImg:(UIImage *)img{
    [_rightBtn setImage:img forState:UIControlStateNormal];
}

- (void)setRightBtnHidden:(BOOL)isHidden{
    _rightBtn.hidden = isHidden;
    if(isHidden){
        _rightBtnWidthConstraint.constant = 0;
    }
}

- (void)setTitleNumberOfLines:(NSInteger)num{
    _titleLbl.numberOfLines = num;
}

- (void)setContentNumberOfLines:(NSInteger)num{
    _contentLbl.numberOfLines = num;
}

- (void)setTitleFont:(UIFont *)titleFont{
    _titleLbl.font = titleFont;
}

- (void)setContentFont:(UIFont *)contentFont{
    _contentLbl.font = contentFont;
}

- (void)setTitleColor:(UIColor *)titleColor{
    _titleLbl.textColor = titleColor;
}

- (void)setContentColor:(UIColor *)contentColor{
    _contentLbl.textColor = contentColor;
}



@end
