//
//  ExploreCellView.m
//  Soliton_UI
//
//  Created by Joyce Tam on 1/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "ExploreCellView.h"
#import "SOLImageView.h"

@interface ExploreCellView ()
@property (strong, nonatomic) IBOutlet SOLImageView *coverIV;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@end

@implementation ExploreCellView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        [self setSubView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setSubView];
    }
    return self;
}


- (void)setSubView{
    UIView* xibView = [[[NSBundle mainBundle] loadNibNamed:@"ExploreCellView" owner:self options:nil] objectAtIndex:0];
    [xibView setFrame:[self bounds]];
    [self addSubview:xibView];
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title subTitle:(NSString *)subTitle{
    [_coverIV setImageWithImageUrl:imgUrl];
    _titleLbl.text = title;
    _subTitleLbl.text = subTitle;
}

- (IBAction)playBtnAction:(id)sender {
    if (self.exploreCellViewPlayButtonDidTap) {
        self.exploreCellViewPlayButtonDidTap(sender);
    }
}

@end
