//
//  VideoTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString* VideoTableViewCellIdentifier;

@interface VideoTableViewCell : UITableViewCell

@property (nonatomic, copy) void (^videoTableViewCellRightButtonDidTap)(VideoTableViewCell *cell, UIButton *sender);

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content;

@end
