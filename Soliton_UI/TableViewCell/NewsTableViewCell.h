//
//  NewsTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 4/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content;

@end
