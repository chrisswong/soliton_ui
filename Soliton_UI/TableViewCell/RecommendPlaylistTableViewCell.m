//
//  RecommendPlaylistTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 3/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "RecommendPlaylistTableViewCell.h"
#import "SOLMultiLineLabel.h"
#import "SOLImageView.h"

@interface RecommendPlaylistTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLImageView *bgIV;
@property (strong, nonatomic) IBOutlet SOLMultiLineLabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIButton *playBtn;

@end

@implementation RecommendPlaylistTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title {
    [_bgIV setImageWithImageUrl:imgUrl];
    _titleLbl.text = title;
}

- (IBAction)playBtnAction:(id)sender {
    if (self.recommendPlayListTableViewCellPLayButtonDidTap) {
        self.recommendPlayListTableViewCellPLayButtonDidTap(self, sender);
    }
}

@end
