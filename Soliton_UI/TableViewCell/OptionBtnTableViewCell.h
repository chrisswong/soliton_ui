//
//  OptionBtnTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 28/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OptionBtnTableViewCell : UITableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier buttonOnImageArray:(NSArray *)btnOnArray buttonOffImageArray:(NSArray *)btnOffArray;

@property (nonatomic, copy) void (^buttonOptionTableViewCellButtonDidTap)(OptionBtnTableViewCell *cell, UIButton *sender, NSInteger index);

@end
