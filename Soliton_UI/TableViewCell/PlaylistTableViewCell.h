//
//  PlaylistTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBAutoScrollLabel.h"
#import "ChartTableViewCell.h"

@interface PlaylistTableViewCell : UITableViewCell

@property (nonatomic, copy) void (^playListTableViewCellRightButtonDidTap)(PlaylistTableViewCell *cell, UIButton *sender);

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload;


@end
