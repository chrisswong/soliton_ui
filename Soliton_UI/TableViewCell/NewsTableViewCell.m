//
//  NewsTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 4/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "NewsTableViewCell.h"
#import "SOLTableViewCellWithImageNonAnimateView.h"

@interface NewsTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLTableViewCellWithImageNonAnimateView *cellView;
@end

@implementation NewsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content{
    [_cellView configCellWithImgUrl:imgUrl title:title content:content isCircleImage:NO];
    [_cellView.rightContentView setTitleNumberOfLines:1];
    [_cellView.rightContentView setContentNumberOfLines:2];
    [_cellView.rightContentView setRightBtnHidden:YES];
}

@end
