//
//  PlaylistTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "PlaylistTableViewCell.h"
#import "SOLTableViewCellAnimateView.h"
#import "SOLImageView.h"

@interface PlaylistTableViewCell ()

@property (strong, nonatomic) IBOutlet SOLImageView *cellIV;
@property (strong, nonatomic) IBOutlet SOLTableViewCellAnimateView *rightContentView;
@end

@implementation PlaylistTableViewCell

- (void)awakeFromNib {
    // Initialization code
    __weak typeof(self) weakSelf = self;
    [_rightContentView setSolTableViewCellAnimateViewRightButtonDidTap:^(UIButton *button) {
        if (weakSelf.playListTableViewCellRightButtonDidTap) {
            weakSelf.playListTableViewCellRightButtonDidTap(weakSelf, button);
        }
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload{
    [_cellIV setImageWithImageUrl:imgUrl];
    [_rightContentView configCellWithTitle:title content:content isHQ:isHQ isDownload:isDownload];
    
}

@end
