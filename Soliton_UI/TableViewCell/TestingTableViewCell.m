//
//  TestingTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 28/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "TestingTableViewCell.h"
#import "GTUIViewAdditions.h"

@implementation TestingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setLbl:(NSString *)testingLbl{
    _testingLbl.text = testingLbl;
}

+ (CGFloat) requiredHeight:(NSString*)text{
    TestingTableViewCell *cell = [TestingTableViewCell gt_viewFromNib];
    [cell setLbl:text];
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return height;
}

@end
