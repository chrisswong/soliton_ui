//
//  ChartTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 29/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    ChartIndicatorTypeNone,
    ChartIndicatorTypeNew,
    ChartIndicatorTypeUp,
    ChartIndicatorTypeDown,
    ChartIndicatorTypeUnchange
} typedef ChartIndicatorType;

@interface ChartTableViewCell : UITableViewCell

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content ranking:(NSInteger)ranking indicatorType:(ChartIndicatorType)indicatorType;

@property (nonatomic, copy) void (^chartTableViewCellRightButtonDidTap)(ChartTableViewCell *cell, UIButton *sender);

@end
