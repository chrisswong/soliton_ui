//
//  EditTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 5/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditTableViewCell : UITableViewCell

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload;
    
@property (nonatomic, copy) void (^editTableViewCellSelectButtonDidTap)(EditTableViewCell *cell, UIButton *sender);

@end
