//
//  VideoTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "VideoTableViewCell.h"
#import "SOLTableViewCellAnimateView.h"
#import "SOLImageView.h"
#import "SOLTableViewCellWithImageNonAnimateView.h"

NSString* VideoTableViewCellIdentifier = @"VideoTableViewCellIdentifier";

@interface VideoTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLTableViewCellWithImageNonAnimateView *cellView;
@end

@implementation VideoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    __weak typeof(self) weakSelf = self;
    [_cellView setSolTableViewCellWithImageNonAnimateViewRightButtonDidTap:^(UIButton *button) {
        if (weakSelf.videoTableViewCellRightButtonDidTap) {
            weakSelf.videoTableViewCellRightButtonDidTap(weakSelf, button);
        }
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content{
    [_cellView configCellWithImgUrl:imgUrl title:title content:content isCircleImage:NO];
    [_cellView.rightContentView setTitleNumberOfLines:1];
    [_cellView.rightContentView setContentNumberOfLines:1];
    [_cellView setRectangleImageRatio:0.5];
}


@end
