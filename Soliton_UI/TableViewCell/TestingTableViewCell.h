//
//  TestingTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 28/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *testingLbl;

-(void)setLbl:(NSString *)testingLbl;

+ (CGFloat) requiredHeight:(NSString*)text;

@end
