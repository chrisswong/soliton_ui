//
//  SOLHeaderTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 3/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLHeaderTableViewCell.h"
@interface SOLHeaderTableViewCell ()
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UIButton *rightBtn;
@end

@implementation SOLHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithTitle:(NSString *)title{
    [self configCellWithTitle:title totalNum:0 rightBtnType:HeaderRightBtnTypeNone];
}

- (void)configCellWithTitle:(NSString *)title totalNum:(NSInteger)totalNum{
    [self configCellWithTitle:title totalNum:totalNum rightBtnType:HeaderRightBtnTypeNone];
}

- (void)configCellWithTitle:(NSString *)title totalNum:(NSInteger)totalNum rightBtnType:(HeaderRightBtnType)rightBtnType{
    _titleLbl.text = [title uppercaseString];
    if (totalNum > 0) {
        _titleLbl.text = [_titleLbl.text stringByAppendingFormat:@" (%ld)",(long)totalNum];
    }
    _rightBtn.hidden = NO;
    
    NSString *imgName;
    switch (rightBtnType) {
        case HeaderRightBtnTypeMore:
            imgName = @"general_btn_more2";
            break;
        default:
            _rightBtn.hidden = YES;
            break;
    }
    
    if (!_rightBtn.hidden) {
        [_rightBtn setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    }
}

- (IBAction)rightBtnAction:(id)sender {
    if (self.solHeaderTableViewCellRightButtonDidTap) {
        self.solHeaderTableViewCellRightButtonDidTap(self, sender);
    }
}

@end
