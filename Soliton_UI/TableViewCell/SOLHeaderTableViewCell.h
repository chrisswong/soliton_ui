//
//  SOLHeaderTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 3/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>
enum {
    HeaderRightBtnTypeNone,
    HeaderRightBtnTypeMore,
    HeaderRightBtnTypeSortTracks,
    HeaderRightBtnTypeClearAll,
    HeaderRightBtnTypeClear,
    HeaderRightBtnTypeEditPlaylists
} typedef HeaderRightBtnType;

@interface SOLHeaderTableViewCell : UITableViewCell

- (void)configCellWithTitle:(NSString *)title totalNum:(NSInteger)totalNum rightBtnType:(HeaderRightBtnType)rightBtnType;
- (void)configCellWithTitle:(NSString *)title totalNum:(NSInteger)totalNum;
- (void)configCellWithTitle:(NSString *)title;

@property (nonatomic, copy) void (^solHeaderTableViewCellRightButtonDidTap)(SOLHeaderTableViewCell *cell, UIButton *sender);

@end
