//
//  RadioTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 1/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "RadioTableViewCell.h"
#import "SOLImageView.h"

@interface RadioTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLImageView *cellIV;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@end

@implementation RadioTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title{
    [_cellIV setImageWithImageUrl:imgUrl];
    _titleLbl.text = title;
}

@end
