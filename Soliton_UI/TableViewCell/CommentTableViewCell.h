//
//  CommentTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title subtitle:(NSString *)subtitle content:(NSString *)content;
+ (CGFloat) requiredHeightWithTitle:(NSString *)title subtitle:(NSString *)subtitle content:(NSString *)content;
- (void)setContentNumberOfLines:(NSInteger)num;

@end
