//
//  EditTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 5/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "EditTableViewCell.h"
#import "SOLTableViewCellAnimateView.h"
@interface EditTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLTableViewCellAnimateView *rightContentView;
@end

@implementation EditTableViewCell

- (void)awakeFromNib {
    // Initialization code
     [_rightContentView setRightBtnImg:[UIImage imageNamed:@"general_edit_btn_move"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content isHQ:(BOOL)isHQ isDownload:(BOOL)isDownload{
    [_rightContentView configCellWithTitle:title content:content isHQ:isHQ isDownload:isDownload];
    
}

- (IBAction)selectBtnAction:(id)sender {
    UIButton *selectBtn = sender;
    [selectBtn setSelected:!selectBtn.selected];

    if (self.editTableViewCellSelectButtonDidTap) {
        self.editTableViewCellSelectButtonDidTap(self, sender);
    }
}

@end
