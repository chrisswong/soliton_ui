//
//  ChartTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 29/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "ChartTableViewCell.h"
#import "SOLImageView.h"
#import "SOLTableViewCellWithImageNonAnimateView.h"

@interface ChartTableViewCell ()


@property (strong, nonatomic) IBOutlet SOLTableViewCellWithImageNonAnimateView *rightContentView;
@property (strong, nonatomic) IBOutlet UILabel *rankingLbl;
@property (strong, nonatomic) IBOutlet UIImageView *chartIndicatorIV;
@end

@implementation ChartTableViewCell

- (void)awakeFromNib {
    // Initialization code    
    __weak typeof(self) weakSelf = self;
    [_rightContentView setSolTableViewCellWithImageNonAnimateViewRightButtonDidTap:^(UIButton *button) {
        if (weakSelf.chartTableViewCellRightButtonDidTap) {
            weakSelf.chartTableViewCellRightButtonDidTap(weakSelf, button);
        }
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content ranking:(NSInteger)ranking indicatorType:(ChartIndicatorType)indicatorType{
    [_rightContentView configCellWithImgUrl:imgUrl title:title content:content isCircleImage:NO];
    [_rightContentView.rightContentView setTitleNumberOfLines:1];
    [_rightContentView.rightContentView setContentNumberOfLines:1];
    
    _rankingLbl.text = [NSString stringWithFormat:@"%ld",(long)ranking];
    NSString *imgName;
    switch (indicatorType) {
        case ChartIndicatorTypeNew:
            imgName = @"explore_chart_img_new";
            break;
        case ChartIndicatorTypeUp:
            imgName = @"explore_chart_img_up";
            break;
        case ChartIndicatorTypeDown:
            imgName = @"explore_chart_img_down";
            break;
        case ChartIndicatorTypeUnchange:
            imgName = @"explore_chart_img_unchange";
            break;
        default:
            break;
    }
    _chartIndicatorIV.image = [UIImage imageNamed:imgName];
}

@end
