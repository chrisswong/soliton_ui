//
//  CommentTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 27/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "GTUIViewAdditions.h"
#import "SOLMultiLineLabel.h"
#import "SOLCircleImageView.h"

@interface CommentTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLCircleImageView *profileView;
@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLbl;
@property (strong, nonatomic) IBOutlet SOLMultiLineLabel *contentLbl;
@end

@implementation CommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title subtitle:(NSString *)subtitle content:(NSString *)content{
    [_profileView setImageWithImageUrl:imgUrl];
    _titleLbl.text = title;
    _subTitleLbl.text = subtitle;
    _contentLbl.text = content;
}

+ (CGFloat) requiredHeightWithTitle:(NSString *)title subtitle:(NSString *)subtitle content:(NSString *)content{
    CommentTableViewCell *cell = [CommentTableViewCell gt_viewFromNib];
    [cell configCellWithImgUrl:nil title:title subtitle:subtitle content:content];
    CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    return height + 1;
}

- (void)setContentNumberOfLines:(NSInteger)num{
    _contentLbl.numberOfLines = num;
}

@end
