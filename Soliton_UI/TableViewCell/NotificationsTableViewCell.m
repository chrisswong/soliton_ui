//
//  NotificationsTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 4/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "NotificationsTableViewCell.h"
#import "SOLTableViewCellWithImageNonAnimateView.h"
#import "GTUIViewAdditions.h"

@interface NotificationsTableViewCell ()
@property (strong, nonatomic) IBOutlet SOLTableViewCellWithImageNonAnimateView *cellView;
@end

@implementation NotificationsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title content:(NSString *)content{
    [_cellView configCellWithImgUrl:imgUrl title:title content:content isCircleImage:YES];
    [_cellView.rightContentView setTitleNumberOfLines:1];
    [_cellView.rightContentView setContentNumberOfLines:1];
}


@end
