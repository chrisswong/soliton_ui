//
//  OptionBtnTableViewCell.m
//  Soliton_UI
//
//  Created by Joyce Tam on 28/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "OptionBtnTableViewCell.h"

@implementation OptionBtnTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier buttonOnImageArray:(NSArray *)btnOnArray buttonOffImageArray:(NSArray *)btnOffArray
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        int maxX=0;
        double width = [[UIScreen mainScreen] applicationFrame].size.width/btnOnArray.count;
        for (int i=0; i<btnOnArray.count; i++) {
            UIButton *optionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            optionBtn.frame = CGRectMake(maxX, 0, width, 50);
            [optionBtn setImage:[UIImage imageNamed:[btnOffArray objectAtIndex:i]] forState:UIControlStateNormal];
            [optionBtn setImage:[UIImage imageNamed:[btnOnArray objectAtIndex:i]] forState:UIControlStateSelected];
            optionBtn.backgroundColor = [UIColor clearColor];
            optionBtn.tag = i;
            [optionBtn addTarget:self action:@selector(optionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.contentView addSubview:optionBtn];
            maxX += width;
        }
        self.contentView.backgroundColor = [UIColor grayColor];
        
    }
    return self;
}

- (void)optionButtonAction:(id)sender {
    UIButton *selectBtn = sender;
    selectBtn.selected = !selectBtn.selected;
    if (self.buttonOptionTableViewCellButtonDidTap) {
        self.buttonOptionTableViewCellButtonDidTap(self, sender, selectBtn.tag);
    }
}

@end
