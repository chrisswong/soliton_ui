//
//  RecommendPlaylistTableViewCell.h
//  Soliton_UI
//
//  Created by Joyce Tam on 3/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendPlaylistTableViewCell : UITableViewCell

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title;

@property (nonatomic, copy) void (^recommendPlayListTableViewCellPLayButtonDidTap)(RecommendPlaylistTableViewCell *cell, UIButton *sender);

@end
