//
//  GTCache.h
//  GTFoundation
//
//  Created by Wai Cheung on 25/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTCache : NSObject

+ (GTCache *)sharedCache;

/**
 *  Check if data is expired for time
 *
 *  @param key     key to retrive data
 *  @param second  expired period
 *
 *  @return Boolean if YES, data is expired else is valid
 */
- (BOOL)isDataForKey:(NSString *)key expiredForTime:(NSTimeInterval)second;

/**
 *  Check if data is cached without time limitation
 *
 *  @param key     key to retrive data
 *
 *  @return Boolean if YES, data is cached
 */
- (BOOL)isCached:(NSString *)key;

/**
 *  Path for data stored
 *
 *  @param key     key to retrive data
 *
 *  @return NSString data stored path
 */
- (NSString *)pathForDataFile:(NSString *)key;

/**
 *  Store data by key (sync)
 *
 *  @param data data to be stored
 *  @param key  key to retrive data
 */
- (void)storeData:(NSData *)data key:(NSString *)key;

/**
 *  Store object by key (sync)
 *
 *  @param object object that can be archived
 *  @param key    key to retrive data
 */
- (void)storeObject:(id)object key:(NSString *)key;

/**
 *  Store data by key (async)
 *
 *  @param data       data to be stored
 *  @param key        key to retrive data
 *  @param completion callback when storing data completed
 */
- (void)storeData:(NSData *)data key:(NSString *)key completion:(void(^)(NSData *data, NSString *key))completion;

/**
 *  Store object by key (async)
 *
 *  @param object     object that can be archived
 *  @param key        key to retrive data
 *  @param completion callback when storing data completed
 */
- (void)storeObject:(id)object key:(NSString *)key completion:(void(^)(id object, NSString *key))completion;

/**
 *  Get stored data by key (sync)
 *
 *  @param key key to stored data
 *
 *  @return NSData data return from a particular key
 */
- (NSData*)dataForKey:(NSString *)key;

/**
 *  Get stored object by key (sync)
 *
 *  @param key key to stored object
 *
 *  @return id object return from a particular key
 */
- (id)objectForKey:(NSString *)key;

/**
 *  Get stored data by key (asycn)
 *
 *  @param key        key to stored data
 *  @param completion callback when data retrived from disk completion
 */
- (void)dataForKey:(NSString *)key completion:(void(^)(NSData *data, NSString *key))completion;

/**
 *  Get stored object by key (sync)
 *
 *  @param key        key to stored object
 *  @param completion callback when object retrived from disk completion
 */
- (void)objectForKey:(NSString *)key completion:(void(^)(id object, NSString *key))completion;
/**
 *  remove a cached data
 *
 *  @param key key to remove data
 */
- (void)removeCacheForKey:(NSString *)key;

/**
 *  remove all cached data
 */
- (void)removeAllCaches;

@end
