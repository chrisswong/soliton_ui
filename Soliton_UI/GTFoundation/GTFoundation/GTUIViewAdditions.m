//
//  GTUIViewAdditions.m
//  GTFoundation
//
//  Created by Wai Cheung on 12/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTUIViewAdditions.h"

@implementation UIView (GTUIViewAdditions_frame)


- (CGFloat)left {
    return self.frame.origin.x;
}


- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}


- (CGFloat)top {
    return self.frame.origin.y;
}


- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}


- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}


- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}


- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}


- (CGFloat)centerX {
    return self.center.x;
}


- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}


- (CGFloat)centerY {
    return self.center.y;
}


- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}


- (CGFloat)width {
    return self.frame.size.width;
}


- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}


- (CGFloat)height {
    return self.frame.size.height;
}


- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

- (CGPoint)origin {
    return self.frame.origin;
}


- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}


- (CGSize)size {
    return self.frame.size;
}


- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}

- (void)gt_roundOffFrame{
    self.frame = CGRectIntegral(self.frame);
}


- (CGRect)gt_contentFrame {
    return CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
}

- (void)gt_useAutolayout {
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

@end


@implementation UIView (GTUIViewAdditions_hierarchy)

- (void)gt_removeAllSubviews {
    while (self.subviews.count) {
        UIView* child = self.subviews.lastObject;
        [child removeFromSuperview];
    }
}


- (NSInteger)gt_getSubviewIndex {
    return [self.superview.subviews indexOfObject:self];
}

- (void)gt_bringToFront {
    [self.superview bringSubviewToFront:self];
}

- (void)gt_sendToBack {
    [self.superview sendSubviewToBack:self];
}

- (void)gt_bringOneLevelUp {
    NSInteger currentIndex = [self gt_getSubviewIndex];
    [self.superview exchangeSubviewAtIndex:currentIndex withSubviewAtIndex:currentIndex+1];
}

- (void)gt_sendOneLevelDown {
    NSInteger currentIndex = [self gt_getSubviewIndex];
    [self.superview exchangeSubviewAtIndex:currentIndex withSubviewAtIndex:currentIndex-1];
}

- (BOOL)gt_isInFront {
    return ([self.superview.subviews lastObject]==self);
}

- (BOOL)gt_isAtBack {
    return ([self.superview.subviews objectAtIndex:0]==self);
}

- (void)gt_swapDepthsWithView:(UIView *)swapView {
    [self.superview exchangeSubviewAtIndex:[self gt_getSubviewIndex] withSubviewAtIndex:[swapView gt_getSubviewIndex]];
}



@end


@implementation UIView (GTUIViewAdditions_debug)

- (void)gt_showDebugBorder {
#ifdef DEBUG
    // random color
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = 1;
    self.layer.masksToBounds = YES;
#endif
}

- (void)gt_showDebugBorderWithColor:(UIColor *)color {
#ifdef DEBUG
    self.layer.borderColor = color.CGColor;
    self.layer.borderWidth = 1;
    self.layer.masksToBounds = YES;
#endif
}

@end


@implementation UIView (GTUIViewAdditions_nib)

+ (id)gt_viewFromNib {
    
    return [[self class] gt_viewFromNib:NSStringFromClass(self.class)];
}

+ (id)gt_viewFromNib:(NSString *)nibName {
    
    NSArray *objs = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    for (id obj in objs) {
        if ([obj isKindOfClass:self.class]) {
            return obj;
        }
    }
    return nil;
}

+ (UINib *)gt_uinib {
    
    return [self gt_uinibWithName:NSStringFromClass(self.class)];
}

+ (UINib *)gt_uinibWithName:(NSString *)nibName {
    
    return [UINib nibWithNibName:nibName bundle:nil];
}

+ (id)gt_viewFromUINib:(UINib *)nib {
    
    NSArray *objs = [nib instantiateWithOwner:nil options:nil];
    for (id obj in objs) {
        if ([obj isKindOfClass:self.class]) {
            return obj;
        }
    }
    return nil;
}

@end


@implementation UIView (GTUIViewAdditions_appearence)

- (void)gt_addCircleCornerRadius {
    self.layer.cornerRadius = self.size.height*.5;
    self.layer.masksToBounds = YES;
}

- (void)gt_addCornerRadius:(float)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

@end

