//
//  GTUIAlertController.h
//  GTFoundation
//
//  Created by Wai Cheung on 10/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GTUIAlertController : NSObject
@property (strong, nonatomic, readonly) id alert;
@property (strong, nonatomic) NSDictionary *userInfo;
@property (copy, nonatomic) void(^completionBlock)(id alert, NSDictionary *userInfo, BOOL isCancelled, NSInteger buttonIndex);

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherTitles
                     userInfo:(NSDictionary *)userInfo
                 useAlertView:(BOOL)useAlertView
                   completion:(void(^)(id alert, NSDictionary *userInfo, BOOL isCancelled, NSInteger buttonIndex))completion;

- (void)show;

- (void)showAtViewController:(UIViewController *)viewController;

@end
