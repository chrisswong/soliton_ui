//
//  GT_UIImageAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 11/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import UIKit;

@interface UIImage (GTUIImageAdditions_image_resize)

/**
 *  make UIImage to be resizable and stretch content at center
 *
 *  @return UIImage 
 */
- (UIImage *)gt_resizeImageAtCenter;

@end
