//
//  GTBarCodeScanner.m
//  UITester
//
//  Created by Wai Cheung on 17/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTBarCodeScanner.h"

@interface GTBarCodeScanner () <AVCaptureMetadataOutputObjectsDelegate> {
    BOOL _isPauseFiring;
}
@property (nonatomic, strong, readonly) AVCaptureSession *captureSession;
@property (nonatomic, strong, readonly) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (strong, nonatomic) NSTimer *retryTimer;
@end

@implementation GTBarCodeScanner

+ (instancetype)scannerWithPreviewView:(UIView *)previewView
                   metadataObjectTypes:(NSArray *)metadataObjectTypes
                    scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack {
    return [[GTBarCodeScanner alloc] initWithPreviewView:previewView metadataObjectTypes:metadataObjectTypes scanResultCallBack:scanResultCallBack];
}

- (instancetype)initWithPreviewView:(UIView *)previewView
                metadataObjectTypes:(NSArray *)metadataObjectTypes
                 scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack {
    if ((self = [super init])) {
        _previewView = previewView;
        _metadataObjectTypes = metadataObjectTypes;
        
        NSAssert(_previewView != nil, @"`previewView` cannot be nil.");
        NSAssert(_metadataObjectTypes != nil, @"`metadataObjectTypes` cannot be nil.");
        
        self.scanResultCallBack = scanResultCallBack;
        
        [self _commonInit];
    }
    return self;
}

- (void)_commonInit {
    _isPauseFiring = NO;
    _isScanning = NO;
    _enableBarCodeDetection = YES;
    _retryIdleTime = 2;
}

- (void)_resumeDetection:(NSTimer *)timer{
    [_retryTimer invalidate];
    self.retryTimer = nil;
    _isPauseFiring = NO;
}

- (BOOL)startScanning {
    NSError *error;
    
    if (_isScanning) return NO;
    
    // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video
    // as the media type parameter.
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Get an instance of the AVCaptureDeviceInput class using the previous device object.
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        // If any error occurs, simply log the description of it and don't continue any more.
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    // Initialize the captureSession object.
    _captureSession = [[AVCaptureSession alloc] init];
    // Set the input device on the capture session.
    [_captureSession addInput:input];
    
    
    // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    // Create a new serial dispatch queue.
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_previewView.layer.bounds];
    [_previewView.layer addSublayer:_videoPreviewLayer];
    
    
    // Start video capture.
    [_captureSession startRunning];
    
    
    _isScanning = YES;
    [self _resumeDetection:nil];
    
    return YES;
}

- (void)stopScanning {
    // Stop video capture and make the capture session object nil.
    [_captureSession stopRunning];
    _captureSession = nil;
    
    _isScanning = NO;
    
    // Remove the video preview layer from the viewPreview view's layer.
    [_videoPreviewLayer removeFromSuperlayer];
}

- (void)dealloc {
    [self stopScanning];
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate method implementation
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    
    // Check if the metadataObjects array is not nil and it contains at least one object.
    if (metadataObjects != nil && [metadataObjects count] > 0 && _enableBarCodeDetection && !_isPauseFiring) {
        // Get the metadata object.
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.scanResultCallBack) {
                self.scanResultCallBack(metadataObj.stringValue, metadataObj);
            }
            
            
            // resume timer
            if (self.retryTimer) {
                [_retryTimer invalidate];
                self.retryTimer = nil;
            }
            self.retryTimer = [NSTimer scheduledTimerWithTimeInterval:_retryIdleTime target:self selector:@selector(_resumeDetection:) userInfo:nil repeats:NO];
            
        });
        
        _isPauseFiring = YES;
    }
    
}

@end


@implementation GTQRCodeScanner
+ (instancetype)scannerWithPreviewView:(UIView *)previewView
                    scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack {
    return [super scannerWithPreviewView:previewView metadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode] scanResultCallBack:scanResultCallBack];
}

- (instancetype)initWithPreviewView:(UIView *)previewView
                 scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack {
    return [super initWithPreviewView:previewView metadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode] scanResultCallBack:scanResultCallBack];
}
@end
