//
//  GTUIViewControllerAdditions.h
//  GTFoundation
//
//  Created by Cheung Chun Wai on 22/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (GTUIViewControllerAdditions)



/**
 *  create a view controller from nib
 *  @return UIViewController
 */
+ (instancetype)gt_viewControllerFromNib;

/**
 *  create a view controller from nib with suffix
 *  @param  NSString file name suffix
 *  @return UIViewController
 */
+ (instancetype)gt_viewControllerFromNibWithSuffix:(NSString *)suffix;

/**
 *  get current view controller index from navigation controller
 *  @return get current view controller index
 */
- (NSInteger)gt_currentViewControllerIndex;

/**
 *  get previous view controller from navigation controller
 *  @return UIView first responder
 */
- (UIViewController *)gt_previousViewController;

/**
 *  find first responder in view controller
 *  @return UIView first responder
 */
- (UIView *)gt_findFirstResponder;

/**
 *  add child view controller to view
 *  @param  UIView view to contain child view controller
 */
- (void)gt_addChildViewController:(UIViewController *)childController toView:(UIView *)view;


/**
 *  add child view controller to view
 *  @param  Callback to modify child view controller layout
 *  @param  UIView view to contain child view controller
 */
- (void)gt_addChildViewController:(UIViewController *)childController toView:(UIView *)view updateChildControllerLayout:(void(^)(UIView *childView, UIView *toView))updateChildControllerLayoutBlock;

/**
 *  remove viewcontroller for parent view controller safely
 */
- (void)gt_removeFromSuperViewController;

@end
