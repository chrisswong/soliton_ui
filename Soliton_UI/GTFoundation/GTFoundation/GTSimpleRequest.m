//
//  GTSimpleRequest.m
//
//  Created by Wai Cheung on 27/12/13.
//  Copyright (c) 2013 Green Tomato Limited. All rights reserved.
//

#import "GTSimpleRequest.h"
#import "GTCommon.h"

#ifdef DEBUG
#define kIsEnableLogger  YES
#else
#define kIsEnableLogger  NO
#endif

static const NSTimeInterval kTimeoutInterval = 60.0;
static NSString *kMultipartFormBoundary = @"----756B5B3756B5B3";

#pragma mark - Private Additions
////////////////////////////////////////////////////////////////////////////
@interface NSString (GTSimpleRequest_URLEncoding)
- (NSString *)URLEncodedString;
- (NSString *)URLDecodedString;
@end

@implementation NSString (GTSimpleRequest_URLEncoding)
- (NSString *)URLEncodedString
{
    __autoreleasing NSString *encodedString;
    NSString *originalString = (NSString *)self;
    encodedString = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                          NULL,
                                                                                          (__bridge CFStringRef)originalString,
                                                                                          NULL,
                                                                                          (CFStringRef)@":!*();@/&?#[]+$,='%’\"",
                                                                                          kCFStringEncodingUTF8
                                                                                          );
    return encodedString;
}
- (NSString *)URLDecodedString
{
    __autoreleasing NSString *decodedString;
    NSString *originalString = (NSString *)self;
    decodedString = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(
                                                                                                          NULL,
                                                                                                          (__bridge CFStringRef)originalString,
                                                                                                          CFSTR(""),
                                                                                                          kCFStringEncodingUTF8
                                                                                                          );
    return decodedString;
}
@end

@interface NSDictionary (QueryString)
+ (NSDictionary *)dictionaryWithQueryString:(NSString *)queryString;
- (NSString *)queryStringValue;
- (NSData *)multipartFormDataWithBoundary:(NSString *)boundary;
- (BOOL)hasMultipartFormContent;
@end

@implementation NSDictionary (QueryString)
+ (NSDictionary *)dictionaryWithQueryString:(NSString *)queryString
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSArray *pairs = [queryString componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs)
    {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        if (elements.count == 2)
        {
            NSString *key = elements[0];
            NSString *value = elements[1];
            NSString *decodedKey = [key URLDecodedString];
            NSString *decodedValue = [value URLDecodedString];
            
            if (![key isEqualToString:decodedKey])
                key = decodedKey;
            
            if (![value isEqualToString:decodedValue])
                value = decodedValue;
            
            [dictionary setObject:value forKey:key];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

- (NSString *)queryStringValue
{
    NSMutableArray *pairs = [NSMutableArray array];
    for (NSString *key in [self keyEnumerator])
    {
        id value = [self objectForKey:key];
        if (![value isKindOfClass:[GTSimpleRequestAttachment class]]) {
            NSString *escapedValue = [value URLEncodedString];
            [pairs addObject:[NSString stringWithFormat:@"%@=%@", key, escapedValue]];
        }
    }
    
    return [pairs componentsJoinedByString:@"&"];
}

- (NSData *)multipartFormDataWithBoundary:(NSString *)boundary
{
    NSArray *dictKeys = [self allKeys];
    
    NSMutableData *body = [NSMutableData data];
    
    //Start Boundary
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", boundary];
    
    int i = 0;
    
    for (NSString *key in dictKeys){
        
        id object = [self objectForKey: key];
        
        if ([object isKindOfClass: [GTSimpleRequestAttachment class]] && ((GTSimpleRequestAttachment *)object).fileData.length){
            // handle attachment
            GTSimpleRequestAttachment *attachment = (GTSimpleRequestAttachment *)object;
            
            [body appendData:[[NSString stringWithFormat: @"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", key, attachment.fileName] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData: attachment.fileData];
            
            i++;
            
            //End Boundary
            if (i < [dictKeys count]){
                [body appendData:[endItemBoundary dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        else if (object && ![object isEqual:[NSNull null]]) {
            
            // handle NSNumber or NSString
            [body appendData:[[NSString stringWithFormat: @"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[(NSString *)object dataUsingEncoding: NSUTF8StringEncoding]];
            i++;
            
            //End Boundary
            if (i < [dictKeys count]){
                [body appendData:[endItemBoundary dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding: NSUTF8StringEncoding]];
    
    return body;
}

- (BOOL)hasMultipartFormContent {
    __block BOOL hasMultipartFormContent = NO;
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([obj isKindOfClass:[GTSimpleRequestAttachment class]]) {
            hasMultipartFormContent = YES;
            *stop = YES;
        }
    }];
    return hasMultipartFormContent;
}
@end

#pragma mark - GTSimpleRequest
////////////////////////////////////////////////////////////////////////////
@interface GTSimpleRequest()<NSURLConnectionDelegate>
@property (strong, nonatomic) NSHTTPURLResponse *response;
@property (strong, nonatomic) NSDictionary *cookies;
@property (strong, nonatomic) NSError *error;
@property (strong, nonatomic) NSData *responseData;
@property (strong, nonatomic) NSString *responseString;
@property (strong, nonatomic) NSMutableData *dataReceived;
@property (nonatomic) BOOL isRunning;
@property (nonatomic) NSUInteger bytesReceived;
@property (nonatomic) NSInteger expectedBytes;
@end

@implementation GTSimpleRequest

// global settings
+ (GTSimpleRequest *)proxy{
    static GTSimpleRequest *_proxy;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _proxy = [[GTSimpleRequest alloc] init];
        _proxy.timeoutInterval = kTimeoutInterval;
        _proxy.isEnableLogger = kIsEnableLogger;
    });
    return _proxy;
}

+ (GTSimpleRequest *)GET:(NSString *)urlString
                  params:(NSDictionary *)params
         requestModifier:(void(^)(NSMutableURLRequest *request))requestModifier
                 success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                 failure:(void(^)(NSError *err))failure{
    return [[self alloc] initWithURLString:urlString HTTPMethod:@"GET" params:params requestModifier:requestModifier success:success failure:failure];
}

+ (GTSimpleRequest *)POST:(NSString *)urlString
                   params:(NSDictionary *)params
          requestModifier:(void(^)(NSMutableURLRequest *request))requestModifier
                  success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                  failure:(void(^)(NSError *err))failure{
    return [[self alloc] initWithURLString:urlString HTTPMethod:@"POST" params:params requestModifier:requestModifier success:success failure:failure];
}

- (instancetype)initWithURLString:(NSString *)urlString
                       HTTPMethod:(NSString *)HTTPMethod
                           params:(NSDictionary *)params
                  requestModifier:(void(^)(NSMutableURLRequest *request))requestModifier
                          success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success failure:(void(^)(NSError *err))failure{
    if ((self = [super init])) {
        _isEnableLogger = [[self class] proxy].isEnableLogger;
        
        _urlString = [urlString copy];
        _HTTPMethod = HTTPMethod.uppercaseString;
        _params = [params copy];
        
        NSURL *url;
        // handle get method
        if ([_HTTPMethod.uppercaseString isEqualToString:@"GET"] && _params.count) {
            url = [NSURL URLWithString:[urlString stringByAppendingFormat:@"?%@",[_params queryStringValue]]];
        }else{
            url = [NSURL URLWithString:urlString];
        }
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:[[self class] proxy].timeoutInterval];
        [request setHTTPMethod:_HTTPMethod];
        
        // handle post method
        if ([_HTTPMethod.uppercaseString isEqualToString:@"POST"]) {
            
            if ([params hasMultipartFormContent]) {
                // multipart/form post
                NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",kMultipartFormBoundary];
                [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
                [request setHTTPBody:[params multipartFormDataWithBoundary:kMultipartFormBoundary]];
            } else {
                // url-encoded post
                [request setHTTPBody:[[_params queryStringValue] dataUsingEncoding:NSUTF8StringEncoding]];
            }
        }
        
        // modify request
        if (requestModifier) {
            requestModifier(request);
        }
        
        _successBlock = [success copy];
        _failureBlock = [failure copy];
        
        _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        _dataReceived = [NSMutableData data];
        _isRunning = NO;
    }
    return self;
}

- (void)startRequest{
    
    GTLog(@"connection start \n---\nURL: %@\nParams: %@\nHTTP Method: %@\n---\n\n", self.urlString, self.params, self.HTTPMethod);
    
    self.dataReceived = [NSMutableData data];
    self.responseData = nil;
    self.responseString = nil;
    
    self.expectedBytes = 0;
    self.bytesReceived = 0;
    _progressValue = 0;
    
    // start
    [_connection start];
    self.isRunning = YES;
}

- (void)cancelRequest{
    
    GTLog(@"connection cancel \n---\nURL: %@\nParams: %@\nHTTP Method: %@\n----\n\n", self.urlString, self.params, self.HTTPMethod);
    
    [_connection cancel];
    self.dataReceived = nil;
    self.responseData = nil;
    self.responseString = nil;
    self.isRunning = NO;
    
    self.expectedBytes = 0;
    self.bytesReceived = 0;
    _progressValue = 0;
}

#pragma mark NSURLConnectionDelegate
////////////////////////////////////////////////////////////////////////////
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response {
    self.isRunning = YES;
    self.response = (NSHTTPURLResponse *)response;
    NSString *cookieString = [[_response allHeaderFields] objectForKey:@"Set-Cookie"];
    if (cookieString.length) {
        self.cookies = [[self class] cookiesFromString:cookieString];
    }
    
    
    NSHTTPURLResponse *r = (NSHTTPURLResponse*)response;
    NSDictionary *headers = [r allHeaderFields];
    if (headers){
        if ([headers objectForKey: @"Content-Length"]) {
            self.expectedBytes = [(NSString *)[headers objectForKey: @"Content-Length"] integerValue];
        } else {
            self.expectedBytes = -1;
        }
    }
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    self.isRunning = YES;
    [_dataReceived appendData:data];
    
    NSUInteger receivedLen = [data length];
    self.bytesReceived = (self.bytesReceived + receivedLen);
    
    if(self.expectedBytes != NSURLResponseUnknownLength) {
        _progressValue = MIN(((self.bytesReceived/(double)self.expectedBytes)*100.0)/100.0, 1.0);
        if (self.progressBlock) {
            self.progressBlock(self, _progressValue);
        }
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    self.isRunning = NO;
    self.responseData = [_dataReceived copy];
    self.responseString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    
    GTLog(@"connection finish \n---\nURL: %@\nParams: %@\nHTTP Method: %@\nResponseString: %@\n----\n\n", self.urlString, self.params, self.HTTPMethod, self.responseString);
    
    if(self.successBlock){
        self.successBlock([_response copy], [_cookies copy],[_dataReceived copy]);
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    GTLog(@"connection fail \n---\nURL: %@\nParams: %@\nHTTP Method: %@\nError: %@\n----\n\n", self.urlString, self.params, self.HTTPMethod, error);
    
    self.isRunning = NO;
    self.error = error;
    if(self.failureBlock){
        self.failureBlock(error);
    }
}

#pragma mark Cookies Functions
////////////////////////////////////////////////////////////////////////////
+ (NSString *)stringFromCookies:(NSDictionary *)cookies {
    NSArray *allKeys = cookies.allKeys;
    NSMutableArray *valueStrings = [NSMutableArray array];
    for (NSString *key in allKeys) {
        [valueStrings addObject:[NSString stringWithFormat:@"%@=%@", key, cookies[key]]];
    }
    return (valueStrings.count)? [valueStrings componentsJoinedByString:@"; "] : @"";
}

+ (NSDictionary *)cookiesFromString:(NSString *)cookiesStr {
    NSArray *strarray = [cookiesStr componentsSeparatedByString:@","];
    NSMutableDictionary *mCookies = [NSMutableDictionary dictionary];
    for (NSString *tmpToken in strarray) {
        NSString *strToken = [tmpToken stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSArray *piece = [strToken componentsSeparatedByString:@";"];
        NSArray *pair = [piece[0] componentsSeparatedByString:@"="];
        if (pair.count == 2) {
            mCookies[pair[0]] = pair[1];
        }
    }
    return [mCookies copy];
}

@end


@implementation GTSimpleRequestAttachment
- (instancetype)initWithFileURL:(NSURL *)fileURL {
    if ((self =[super init])) {
        self.fileData = [NSData dataWithContentsOfURL:fileURL];
        self.fileName = fileURL.lastPathComponent;
    }
    return self;
}
@end


#pragma mark GTSimpleRequest_shorthand
////////////////////////////////////////////////////////////////////////////
@implementation NSString(GTSimpleRequest_shorthand)

- (GTSimpleRequest *)gt_GETWithParams:(NSDictionary *)params
                              success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                              failure:(void(^)(NSError *err))failure {
    return [GTSimpleRequest GET:self params:params requestModifier:nil success:success failure:failure];
}

- (GTSimpleRequest *)gt_POSTWithParams:(NSDictionary *)params
                               success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                               failure:(void(^)(NSError *err))failure {
    return [GTSimpleRequest POST:self params:params requestModifier:nil success:success failure:failure];
}

@end

