//
//  GTGradientView.h
//  GTFoundation
//
//  Copyright (c) 2014 Green Tomato. All rights reserved.
//

@import UIKit;
@import QuartzCore;

@interface GTGradientView : UIView
/**
 *  array of CGColor for gradient transition
 *  colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor blackColor].CGColor, (id)[UIColor whiteColor].CGColor];
 */
@property (nonatomic, copy) NSArray* colors;

/**
 *  color positions
 *  locations = @[0.0, 0.5, 1]; // start colors[0] at 0% to colors[1] at 50%, finally change to colors[2] at 100%
 */
@property (nonatomic, copy) NSArray* locations;

/**
 *  start point of gradient
 *  startPoint = CGPoint(0,0); // start at top-left ( x at width * 0.0, y at height * 0.0)
 */
@property (nonatomic) CGPoint startPoint;

/**
 *  end point of gradient
 *  endPoint = CGPoint(1,0.5); // end at center-right ( x at width * 1.0, y at height * 0.5)
 */
@property (nonatomic) CGPoint endPoint;

/**
 *  gradient layer
 *
 *  @return CAGradientLayer
 */
- (CAGradientLayer*)gradientLayer;
@end
