//
//  GTLanguageSupport.m
//  GTFoundation
//	
//  Copyright (c) 2013 Green Tomato. All rights reserved.
//

#import "GTLanguageSupport.h"

NSString *const kGTLanguageSelected = @"kLanguageSelected";

NSString* gt_appLanguage(void) {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kGTLanguageSelected];
}

void gt_setAppLanguage(NSString *lang) {
    [[NSUserDefaults standardUserDefaults] setObject:lang forKey:kGTLanguageSelected];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:kGTLanguageDidChangeNotification object:nil];
}

BOOL gt_isAppLanguage(NSString *lang) {
    return [gt_appLanguage() isEqualToString:lang];
}

NSString* gt_localizedString(NSString* string) {
    return NSLocalizedStringFromTable(string, gt_appLanguage(), @"");
}
