//
//  GTUILabelAdditions.m
//  GTFoundation
//
//  Created by Wai Cheung on 11/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTUILabelAdditions.h"

CGSize gt_UIGetSizeFromString(NSString *string, UIFont *font) {
    
    if (!string || string.length == 0) return CGSizeZero;
    
    CGSize newSize;
    if ([string respondsToSelector:@selector(sizeWithAttributes:)]) {
        
        newSize = [string sizeWithAttributes:@{ NSFontAttributeName: font }];
        
    } else {
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        newSize = [string sizeWithFont:font];
#pragma clang diagnostic pop
        
    }
    
    return newSize;
}

CGSize gt_UIGetSizeFromMultipleLineString(NSString *string, UIFont *font, CGSize constrainedSize) {
    
    if (!string || string.length == 0) return CGSizeZero;
    
    CGSize newSize;
    if ([string respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)]) {
        
        NSDictionary *stringAttributes = @{ NSFontAttributeName: font };
        newSize = [string boundingRectWithSize:constrainedSize
                                       options:/*NSStringDrawingTruncatesLastVisibleLine|*/NSStringDrawingUsesLineFragmentOrigin
                                    attributes:stringAttributes context:nil].size;
        
    } else {
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        newSize = [string sizeWithFont:font constrainedToSize:constrainedSize lineBreakMode:NSLineBreakByWordWrapping];
#pragma clang diagnostic pop
        
    }
    
    return newSize;
}

@implementation UILabel (GTUILabelAdditions_adjust_frame)

- (void)gt_adjustsSizeToFitLabelFont {
    CGSize newSize = gt_UIGetSizeFromString(self.text, self.font);
    CGRect frame = self.frame;
    frame.size = newSize;
    frame = CGRectIntegral(frame);
    self.frame = frame;
}

- (void)gt_adjustsHeightToFitWidthAndFont {
    self.numberOfLines = 0;
    CGRect frame = self.frame;
    CGSize newSize = gt_UIGetSizeFromMultipleLineString(self.text, self.font, CGSizeMake(self.frame.size.width, CGFLOAT_MAX));
    frame.size = CGSizeMake(frame.size.width, newSize.height);
    frame = CGRectIntegral(frame);
    self.frame = frame;
}

- (void)gt_adjustsHeightToFitWidthAndFontWithMaxHeight:(float)maxHeight {
    self.numberOfLines = 0;
    CGRect frame = self.frame;
    CGSize newSize = gt_UIGetSizeFromMultipleLineString(self.text, self.font, CGSizeMake(self.frame.size.width, CGFLOAT_MAX));
    frame.size = CGSizeMake(frame.size.width, MIN(newSize.height, maxHeight));
    frame = CGRectIntegral(frame);
    self.frame = frame;
}

- (void)gt_adjustsHeightToFitWidthAndFontIfNeeded {
    self.numberOfLines = 0;
    CGRect frame = self.frame;
    CGSize newSize = gt_UIGetSizeFromMultipleLineString(self.text, self.font, CGSizeMake(self.frame.size.width, CGFLOAT_MAX));
    frame.size = CGSizeMake(frame.size.width, MAX(self.frame.size.height,newSize.height));
    frame = CGRectIntegral(frame);
    self.frame = frame;
}

@end
