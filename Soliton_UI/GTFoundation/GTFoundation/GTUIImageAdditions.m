//
//  GTUIImageAdditions.m
//  GTFoundation
//
//  Created by Wai Cheung on 11/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTUIImageAdditions.h"

@implementation UIImage (GTUIImageAdditions_image_resize)

- (UIImage *)gt_resizeImageAtCenter {
    
    CGSize size = self.size;
    CGFloat v = floorf((size.height-1)*.5);
    CGFloat h = floorf((size.width-1)*.5);
    if ([self respondsToSelector:@selector(resizableImageWithCapInsets:resizingMode:)]) {
        return [self resizableImageWithCapInsets:UIEdgeInsetsMake(v, h, v, h) resizingMode:UIImageResizingModeStretch];
    }
    else {
        return [self resizableImageWithCapInsets:UIEdgeInsetsMake(v, h, v, h)];
    }
    
}

@end
