//
//  GTObjectStore.h
//  GTWebAppBridgeDemo
//
//  Created by Wai Cheung on 26/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GTObjectStore : NSObject

/**
 *  store identifior
 */
@property (nonatomic, readonly) NSString *name;

/**
 *  app group id
 */
@property (nonatomic, readonly) NSString *appGroupId;

/**
 *  singleton store
 *
 *  @return GTObjectStore
 */
+ (instancetype)sharedStore;

/**
 *  initialize store with name
 *
 *  @param name a name as store identifior
 *
 *  @return GTObjectStore
 */
- (instancetype)initWithName:(NSString *)name;

/**
 *  initialize store with name which shared by the same app group
 *
 *  @param name
 *  @param appGroupId
 *
 *  @return GTObjectStore
 */
- (instancetype)initWithName:(NSString *)name appGroupId:(NSString *)appGroupId;

/**
 *  set object for key
 *
 *  @param obj storing object
 *  @param aKey reference key
 */
- (void)setObject:(id)obj forKey:(id<NSCopying>)aKey;
- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;

/**
 *  get object for key
 *
 *  @param aKey reference key
 *
 *  @return stored object
 */
- (id)objectForKey:(id<NSCopying>)aKey;
- (id)objectForKeyedSubscript:(id <NSCopying>)key;

/**
 *  remove a stored object
 *
 *  @param aKey reference key
 */
- (void)removeObjectForKey:(id<NSCopying>)aKey;

/**
 *  drop all objects from store
 */
- (void)removeAllObjects;

@end
