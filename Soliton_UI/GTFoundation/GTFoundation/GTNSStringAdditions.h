//
//  GTNSStringAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GTNSStringAdditions)

/**
 *  convert string to data (utf8)
 *
 *  @return NSData
 */
- (NSData *)gt_toData;

/**
 *  convert to url
 *
 *  @return NSURL
 */
- (NSURL *)gt_toURL;

/**
 *  convert to fila url
 *
 *  @return NSURL
 */
- (NSURL *)gt_toFileURL;

/**
 *  validate email
 *
 *  @return YES if email is valid
 */
- (BOOL)gt_isValidEmail;

/**
 *  query string from dicitonary
 *
 *  @param dictionary
 *
 *  @return NSString
 */
+ (NSString *)gt_queryFromDictionary:(NSDictionary *)dictionary;

/**
 *  append query from dictionary
 *
 *  @param query query dictionary
 *
 *  @return NSString
 */
- (NSString*)gt_stringByAddingQuery:(NSDictionary*)query;

/**
 *  version (e.g. 1.0.1) comparison
 *
 *  @param other another version
 *
 *  @return NSComparisonResult 
 */
- (NSComparisonResult)gt_versionStringCompare:(NSString *)other;

/**
 *  stripping all HTML tags
 *
 *  @return NSString stripping string
 */
- (NSString *)gt_stringByStrippingHTML;

/**
 *  is string equals others with ignoring case
 *
 *  @param string matching string
 *
 *  @return YES if they are equal
 */
- (BOOL)gt_isEqualIgnoreCaseWithString:(NSString *)string;

@end


@interface NSData (GTNSStringAdditions)

/**
 *  data to string (utf8)
 *
 *  @return NSString
 */
- (NSString *)gt_toString;

/**
 *  data to string with specified encoding
 *
 *  @param encoding
 *
 *  @return NSString
 */
- (NSString *)gt_toStringWithEncoding:(NSStringEncoding)encoding;

@end


/**
 *  JSON related
 */
@interface NSData (GTNSStringAdditions_JSON)

/**
 *  create json object
 *
 *  @return NSArray or NSDictionary
 */
- (id)gt_JSONObject;

/**
 *  create json object (mutable)
 *
 *  @return NSArray or NSDictionary
 */
- (id)gt_JSONMutableObject;

@end

@interface NSString (GTNSStringAdditions_JSON)

/**
 *  create json object
 *
 *  @return NSArray or NSDictionary
 */
- (id)gt_JSONObject;

/**
 *  create json object (mutable)
 *
 *  @return NSArray or NSDictionary
 */
- (id)gt_JSONMutableObject;

@end

@interface NSDictionary (GTNSStringAdditions_JSON)

/**
 *  create json data from dictionary
 *
 *  @return NSData
 */
- (NSData *)gt_toJSONData;

/**
 *  create json string from dictionary
 *
 *  @return NSString
 */
- (NSString *)gt_toJSONString;

@end


@interface NSArray (GTNSStringAdditions_JSON)

/**
 *  create json data from array
 *
 *  @return NSData
 */
- (NSData *)gt_toJSONData;

/**
 *  create json string from array
 *
 *  @return NSString
 */
- (NSString *)gt_toJSONString;

@end

/**
 *  Hash
 */
@interface NSString (GTNSStringAdditions_hash)

/**
 *  MD5 value
 *
 *  @return md5 string
 */
- (NSString *)gt_md5;

/**
 *  SHA1 value
 *
 *  @return sha1 string
 */
- (NSString *)gt_sha1;

/**
 *  SHA256
 *
 *  @return sha256 string
 */
- (NSString *)gt_sha256;

/**
 *  SHA512
 *
 *  @return sha512 string
 */
- (NSString *)gt_sha512;

@end

