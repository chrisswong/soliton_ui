//
//  GTNSDateAdditions.m
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//


#import "GTNSDateAdditions.h"

@implementation NSDateFormatter(GTNSDateAdditions)

+ (NSDateFormatter *)gt_defaultDateFormatter {
    static NSDateFormatter *_dateFomatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dateFomatter = [[NSDateFormatter alloc] init];
    });
    _dateFomatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    return _dateFomatter;
}

+ (NSDateFormatter *)gt_systemDateFormatter {
    NSDateFormatter *currentDateFormatter = [[self class] gt_defaultDateFormatter];
    currentDateFormatter.locale = [NSLocale currentLocale];
    return currentDateFormatter;
}


@end


@implementation NSDate (GTNSDateAdditions_formatter)

- (NSString *)gt_stringWithFormat:(NSString *)format {
    return [self gt_stringWithFormat:format withLocaleId:nil];
}

- (NSString *)gt_stringWithFormat:(NSString *)format withLocaleId:(NSString *)localeId {
    NSDateFormatter *currentDateFormatter = [NSDateFormatter gt_systemDateFormatter];
    if (localeId && localeId.length) {
        currentDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:localeId];
    }
    [currentDateFormatter setDateFormat:format];
    return [currentDateFormatter stringFromDate:self];
}

@end


@implementation NSString (GTNSDateAdditions_string_formatter)

- (NSDate *)gt_dateWithFormat:(NSString *)format {
    return [self gt_dateWithFormat:format withLocaleId:nil];
}

- (NSDate *)gt_dateWithFormat:(NSString *)format withLocaleId:(NSString *)localeId {
    NSDateFormatter *currentDateFormatter = [NSDateFormatter gt_systemDateFormatter];
    if (localeId && localeId.length) {
        currentDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:localeId];
    }
    [currentDateFormatter setDateFormat:format];
    return [currentDateFormatter dateFromString:self];
}

@end


@interface NSDate (GTNSDateAdditions_manipulation_private)
+ (NSCalendar *)_gregorianCalendar;
@end

@implementation NSDate (GTNSDateAdditions_manipulation)

+ (NSCalendar *)_gregorianCalendar {
    return [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
}

- (GTDateInformation)gt_dateInformation{
    return [self gt_dateInformationWithTimeZone:nil];
}

- (GTDateInformation)gt_dateInformationWithTimeZone:(NSTimeZone *)tz {
    GTDateInformation info;
    
    NSCalendar *calendar = [[self class] _gregorianCalendar];
    if( tz ) [calendar setTimeZone:tz];
    NSDateComponents *comps = [calendar components:(NSCalendarUnitMonth | NSCalendarUnitMinute | NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitSecond) fromDate:self];
    
    info.day = [comps day];
    info.month = [comps month];
    info.year = [comps year];
    
    info.hour = [comps hour];
    info.minute = [comps minute];
    info.second = [comps second];
    
    info.weekday = [comps weekday];
    
    return info;
}

+ (NSDate *)gt_dateFromDateInformation:(GTDateInformation)info {
    return [NSDate gt_dateFromDateInformation:info timeZone:nil];
}

+ (NSDate *)gt_dateFromDateInformation:(GTDateInformation)info timeZone:(NSTimeZone *)tz {
    NSCalendar *calendar = [[self class] _gregorianCalendar];
    if( tz ) [calendar setTimeZone:tz];
    NSDateComponents *comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:[NSDate date]];
    
    comps.day = info.day;
    comps.month = info.month;
    comps.year = info.year;
    
    comps.hour = info.hour;
    comps.minute = info.minute;
    comps.second = info.second;
    
    NSDate *date = [calendar dateFromComponents:comps];
    
    return date;
}

+ (NSString *)gt_dateInformationDescriptionWithInformation:(GTDateInformation)info {
    return [NSString stringWithFormat:@"%ld %ld %ld %ld:%ld:%ld", (long)info.month, (long)info.day, (long)info.year, (long)info.hour, (long)info.minute, (long)info.second];
}

+ (NSDate *)gt_yesterday {
    GTDateInformation info = [[NSDate date] gt_dateInformation];
    info.day--;
    return [NSDate gt_dateFromDateInformation:info];
}

+ (NSDate*)gt_month {
    return [[NSDate date] gt_monthDate];
}


+ (NSArray*)gt_datesFromDate:(NSDate*)from toDate:(NSDate*)to {
    NSDate* date = from;
    NSTimeZone* timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSMutableArray* dates = [NSMutableArray arrayWithObject:from];
    while( TRUE ){
        GTDateInformation info = [date gt_dateInformationWithTimeZone:timeZone];
        info.day++;
        date = [NSDate gt_dateFromDateInformation:info timeZone:timeZone];
        if( [date compare:to] == NSOrderedDescending ) break;
        else [dates addObject:date];
    }
    return dates;
}

+ (NSDate *)gt_dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime{
    NSDateFormatter *formatter = [NSDateFormatter gt_defaultDateFormatter];
    
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString* datePortion = [formatter stringFromDate:aDate];
    
    [formatter setDateFormat:@"HH:mm"];
    NSString* timePortion = [formatter stringFromDate:aTime];
    
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSString* dateTime = [NSString stringWithFormat:@"%@ %@", datePortion, timePortion];
    return [formatter dateFromString:dateTime];
}

- (NSDate *)gt_dateByAddingDays:(NSUInteger)days{
    NSDateComponents* comps = [[NSDateComponents alloc] init];
    comps.day = days;
    return [[NSCalendar currentCalendar] dateByAddingComponents:comps toDate:self options:0];
}

- (NSInteger)gt_monthsBetweenDate:(NSDate *)toDate{
    NSCalendar *calendar = [[self class] _gregorianCalendar];
    NSDateComponents *comps = [calendar components:NSCalendarUnitMonth fromDate:[self gt_monthlessDate] toDate:[toDate gt_monthlessDate] options:0];
    NSInteger months = [comps month];
    return ABS(months);
}

- (NSInteger)gt_daysBetweenDate:(NSDate *)toDate{
    NSTimeInterval time = [self timeIntervalSinceDate:toDate];
    return ABS(time / 60 / 60 / 24);
}

- (NSDate*)gt_monthDate{
    NSCalendar *calendar = [[self class] _gregorianCalendar];
    NSDateComponents *comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:self];
    [comps setDay:1];
    NSDate* date = [calendar dateFromComponents:comps];
    return date;
}

- (NSDate *)gt_timelessDate{
    NSCalendar* calendar =  [[self class] _gregorianCalendar];
    NSDateComponents *comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    return [calendar dateFromComponents:comps];
}

- (NSDate *)gt_monthlessDate{
    NSCalendar* calendar =  [[self class] _gregorianCalendar];
    NSDateComponents *comps = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth) fromDate:self];
    return [calendar dateFromComponents:comps];
}

- (NSString *)gt_monthString{
    NSDateFormatter *formatter = [NSDateFormatter gt_systemDateFormatter];
    [formatter setDateFormat:@"MMMM"];
    return [formatter stringFromDate:self];
}

- (NSString *)gt_yearString{
    NSDateFormatter* formatter = [NSDateFormatter gt_systemDateFormatter];
    [formatter setDateFormat:@"yyyy"];
    return [formatter stringFromDate:self];
}

- (BOOL)gt_isSameDay:(NSDate *)anotherDate{
    NSCalendar* calendar = [[self class] _gregorianCalendar];
    NSDateComponents *comps1 = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self];
    NSDateComponents *comps2 = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:anotherDate];
    return ([comps1 year] == [comps2 year] && [comps1 month] == [comps2 month] && [comps1 day] == [comps2 day]);
}

- (BOOL)gt_isToday{
    return [self gt_isSameDay:[NSDate date]];
}

@end