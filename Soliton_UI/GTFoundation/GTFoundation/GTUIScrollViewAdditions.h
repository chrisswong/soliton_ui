// Referenece from iOSKit

@import UIKit;
/**
 This category adds functionality to UIScrollView to automatically determine
 the contentHeight or contentWidth based on it's subviews. It will use the
 greatest x- (for width) or y-value (for height) of any subview that is not marked
 to be excluded from autocalculation (and is not the scrolling indicator).
 
 @see UIView+FKContentSize
 */

@interface UIScrollView (GT_FKContentSize)

/**
 *  auto calculate content height
 */
- (void)gt_autocalculateContentHeight;

/**
 *  auto calculate content height with padding
 */
- (void)gt_autocalculateContentHeightWithPadding:(CGFloat)padding;

/**
 *  auto calculate content width
 */
- (void)gt_autocalculateContentWidth;

/**
 *  auto calculate content width with padding
 */
- (void)gt_autocalculateContentWidthWithPadding:(CGFloat)padding;


/**
 *  auto calculate content width and height
 */
- (void)gt_autocalculateContentSize;

/**
 *  auto calculate content width and height with padding
 */
- (void)gt_autocalculateContentSizeWithPadding:(CGSize)padding;

@end

/**
 *  check if view is scrollView indicator
 */
BOOL gt_FKViewIsScrollIndicator(UIView *view);