//
//  GTBarCodeScanner.h
//  UITester
//
//  Created by Wai Cheung on 17/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import Foundation;
@import UIKit;
@import AVFoundation;



@interface GTBarCodeScanner : NSObject

/**
 *  Camera preview displaying view
 */
@property (nonatomic, strong, readonly) UIView *previewView;

/**
 *  Metadata checking type e.g. AVMetadataObjectTypeQRCode
 */
@property (nonatomic, strong, readonly) NSArray *metadataObjectTypes;

/**
 *  is scanning code or not
 */
@property (nonatomic, readonly) BOOL isScanning;

/**
 *  enable bar code detection
 */
@property (nonatomic) BOOL enableBarCodeDetection;

/**
 *  scan result call back block
 */
@property (nonatomic, copy) void(^scanResultCallBack)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj);

/**
 *  retry after scanned a bar code
 */
@property (nonatomic) NSTimeInterval retryIdleTime;

/**
 *  Code Scanner initializer
 *
 *  @param previewView         preview displaying view
 *  @param metadataObjectTypes Metadaba Object Types
 *  @param scanResultCallBack  result callback
 *
 *  @return GTBarCodeScanner
 */
+ (instancetype)scannerWithPreviewView:(UIView *)previewView
                   metadataObjectTypes:(NSArray *)metadataObjectTypes
                    scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack;

/**
 *  Code Scanner initializer
 *
 *  @param previewView         preview displaying view
 *  @param metadataObjectTypes Metadaba Object Types
 *  @param scanResultCallBack  result callback
 *
 *  @return GTBarCodeScanner
 */
- (instancetype)initWithPreviewView:(UIView *)previewView
                metadataObjectTypes:(NSArray *)metadataObjectTypes
                 scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack;

/**
 *  start scanning
 *
 *  @return YES if scanning open successfully
 */
- (BOOL)startScanning;

/**
 *  stop scanning
 */
- (void)stopScanning;

@end


/**
 *  
 
 Example: 
 
 - (void)viewDidAppear:(BOOL)animated {
     [super viewDidAppear:animated];
     // Do any additional setup after loading the view, typically from a nib.
     self.qrScanner = [GTQRCodeScanner
         scannerWithPreviewView:_scanView
         scanResultCallBack:^(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj) {
             GTLog(@"QR Scanned = %@",result);
     }];
 }
 
 - (IBAction)toggleQR:(id)sender {
     if (!self.qrScanner.isScanning) {
         [self.qrScanner startScanning];
     } else {
         [self.qrScanner stopScanning];
     }
 }
 
 */
@interface GTQRCodeScanner : GTBarCodeScanner

/**
 *  QR Scanner initializer (>=iOS 7)
 *
 *  @param previewView        preview displaying view
 *  @param scanResultCallBack result callback
 *
 *  @return GTQRCodeScanner
 */
+ (instancetype)scannerWithPreviewView:(UIView *)previewView
                    scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack;

/**
 *  QR Scanner initializer (>=iOS 7)
 *
 *  @param previewView        preview displaying view
 *  @param scanResultCallBack result callback
 *
 *  @return GTQRCodeScanner
 */
- (instancetype)initWithPreviewView:(UIView *)previewView
                 scanResultCallBack:(void(^)(NSString *result, AVMetadataMachineReadableCodeObject *metadataObj))scanResultCallBack;
@end