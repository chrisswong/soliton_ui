//
//  GTNSDateAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import UIKit;

/**
 *  GTDateInformation is struct that contain all date components
 */
struct GTDateInformation{
	NSInteger day;
	NSInteger month;
	NSInteger year;
	
	NSInteger weekday;

	NSInteger minute;
	NSInteger hour;
	NSInteger second;
};

typedef struct GTDateInformation GTDateInformation;

@interface NSDateFormatter(GTNSDateAdditions)

/**
 *  singleton date formatter for calulation (always using locale "en_US_POSIX")
 *
 *  @return NSDateFormatter
 */
+ (NSDateFormatter *)gt_defaultDateFormatter;

/**
 *  singleton date formatter for displaying with system locale (always using current locale)
 *
 *  @return NSDateFormatter
 */
+ (NSDateFormatter *)gt_systemDateFormatter;

@end

@interface NSDate (GTNSDateAdditions_date_formatter)

/**
 *  convert date to string in format
 *
 *  @param format display format
 *
 *  @return NSString
 */
- (NSString *)gt_stringWithFormat:(NSString *)format;

/**
 *  convert date to string in format
 *
 *  for the list of localId : https://gist.github.com/jacobbubu/1836273
 *
 *  @param format display format
 *  @param localeId display in locale
 *
 *  @return NSString
 */
- (NSString *)gt_stringWithFormat:(NSString *)format withLocaleId:(NSString *)localeId;

@end

@interface NSString (GTNSDateAdditions_string_formatter)

/**
 *  convert string to date in format
 *
 *  @param format string format
 *
 *  @return NSDate
 */
- (NSDate *)gt_dateWithFormat:(NSString *)format;

/**
 *  convert string to date in format
 *  
 *  for the list of localId : https://gist.github.com/jacobbubu/1836273
 *
 *  @param format string format
 *  @param localeId convert in locale
 *
 *  @return NSDate
 */
- (NSDate *)gt_dateWithFormat:(NSString *)format withLocaleId:(NSString *)localeId;

@end

@interface NSDate (GTNSDateAdditions_manipulation)

/**
 *  get GTDateInformation, GTDateInformation is struct that contain all date components
 *
 *  @return GTDateInformation
 */
- (GTDateInformation)gt_dateInformation;

/**
 *  get GTDateInformation with time zone
 *
 *  @param tz TimeZone
 *
 *  @return GTDateInformation
 */
- (GTDateInformation)gt_dateInformationWithTimeZone:(NSTimeZone *)tz;

/**
 *  convert GTDateInformation to NSDate
 *
 *  @param info GTDateInformation
 *
 *  @return NSDate
 */
+ (NSDate*)gt_dateFromDateInformation:(GTDateInformation)info;

/**
 *  convert GTDateInformation to NSDate with timeZone
 *
 *  @param info GTDateInformation
 *  @param tz TimeZone
 *
 *  @return NSDate
 */
+ (NSDate*)gt_dateFromDateInformation:(GTDateInformation)info timeZone:(NSTimeZone *)tz;

/**
 *  print GTDateInformation
 *
 *  @param info GTDateInformation
 *
 *  @return NSString date information description
 */
+ (NSString*)gt_dateInformationDescriptionWithInformation:(GTDateInformation)info;

/**
 *  get Yesterday
 *
 *  @return NSDate
 */
+ (NSDate *)gt_yesterday;

/**
 *  get 1st day date of current month
 *
 *  @return NSDate
 */
+ (NSDate *)gt_month;

/**
 *  get array of dates between 2 dates
 *
 *  @param from from date
 *  @param to   tod date
 *
 *  @return NSArray dates between 2 dates
 */
+ (NSArray *)gt_datesFromDate:(NSDate *)from toDate:(NSDate *)to;


/**
 *  get date by combining day and time
 *
 *  @param aDate NSDate day part
 *  @param aTime NSDate time part
 *
 *  @return NSDate combined date
 */
+ (NSDate *)gt_dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime;

/**
 *  get date by adding a number of days
 *
 *  @param days
 *
 *  @return NSDate
 */
- (NSDate *)gt_dateByAddingDays:(NSUInteger)days;

/**
 *  get 1st day date of a NSDate object
 *
 *  @return NSDate
 */
- (NSDate *)gt_monthDate;

/**
 *  get date object without time part (started at 00:00:00)
 *
 *  @return NSDate
 */
- (NSDate *)gt_timelessDate;

/**
 *  get date object without month part
 *
 *  @return NSDate
 */
- (NSDate *)gt_monthlessDate;

/**
 *  month name
 *
 *  @return NSString
 */
- (NSString *)gt_monthString;

/**
 *  year name
 *
 *  @return NSString
 */
- (NSString *)gt_yearString;

/**
 *  get number of months between 2 dates
 *
 *  @param toDate
 *
 *  @return NSInteger number of months
 */
- (NSInteger)gt_monthsBetweenDate:(NSDate *)toDate;

/**
 *  get number of days between 2 dates
 *
 *  @param toDate
 *
 *  @return NSInteger number of days
 */
- (NSInteger)gt_daysBetweenDate:(NSDate *)toDate;

/**
 *  is today
 *
 *  @return YES if this date is today
 */
- (BOOL)gt_isToday;


/**
 *  is the same day of another day
 *
 *  @param anotherDate
 *
 *  @return YES if this date is the same day of another day
 */
- (BOOL)gt_isSameDay:(NSDate *)anotherDate;


@end