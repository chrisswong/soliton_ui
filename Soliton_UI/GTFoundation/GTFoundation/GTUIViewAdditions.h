//
//  GTUIViewAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 12/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import UIKit;

@interface UIView (GTUIViewAdditions_frame)

/**
 * Shortcut for frame.origin.x.
 *
 * Sets frame.origin.x = left
 */
@property (nonatomic) CGFloat left;

/**
 * Shortcut for frame.origin.y
 *
 * Sets frame.origin.y = top
 */
@property (nonatomic) CGFloat top;

/**
 * Shortcut for frame.origin.x + frame.size.width
 *
 * Sets frame.origin.x = right - frame.size.width
 */
@property (nonatomic) CGFloat right;

/**
 * Shortcut for frame.origin.y + frame.size.height
 *
 * Sets frame.origin.y = bottom - frame.size.height
 */
@property (nonatomic) CGFloat bottom;

/**
 * Shortcut for frame.size.width
 *
 * Sets frame.size.width = width
 */
@property (nonatomic) CGFloat width;

/**
 * Shortcut for frame.size.height
 *
 * Sets frame.size.height = height
 */
@property (nonatomic) CGFloat height;

/**
 * Shortcut for center.x
 *
 * Sets center.x = centerX
 */
@property (nonatomic) CGFloat centerX;

/**
 * Shortcut for center.y
 *
 * Sets center.y = centerY
 */
@property (nonatomic) CGFloat centerY;

/**
 * Shortcut for frame.origin
 */
@property (nonatomic) CGPoint origin;

/**
 * Shortcut for frame.size
 */
@property (nonatomic) CGSize size;

/**
 *  Round off all half point frame
 */
- (void)gt_roundOffFrame;

/**
 *  get a new rect by using view's size while origin is at (0, 0)
 *
 *  @return CGRect
 */
- (CGRect)gt_contentFrame;

/**
 *  make view ready for autolayout
 */
- (void)gt_useAutolayout;

@end

@interface UIView (GTUIViewAdditions_hierarchy)

/**
 *  remove all subviews
 */
- (void)gt_removeAllSubviews;

/**
 *  get view index from parent view
 *
 *  @return view index
 */
- (NSInteger)gt_getSubviewIndex;

/**
 *  bring view to front
 */
- (void)gt_bringToFront;

/**
 *  sent view to back
 */
- (void)gt_sendToBack;

/**
 *  bring one leve up
 */
- (void)gt_bringOneLevelUp;

/**
 *  bring one leve down
 */
- (void)gt_sendOneLevelDown;

/**
 *  check if view is at the top
 *
 *  @return YES if view is at the top; otherwise, return NO
 */
- (BOOL)gt_isInFront;

/**
 *  check if view is at the back
 *
 *  @return YES if view is at the back; otherwise, return NO
 */
- (BOOL)gt_isAtBack;

/**
 *  swap 2 views
 *
 *  @param swapView swap target
 */
- (void)gt_swapDepthsWithView:(UIView *)swapView;

@end

@interface UIView (GTUIViewAdditions_debug)

/**
 *  show border of a view with random color (only work under DEBUG flag)
 */
- (void)gt_showDebugBorder;

/**
 *  show border of a view with specfied color (only work under DEBUG flag)
 *
 *  @param color border color
 */
- (void)gt_showDebugBorderWithColor:(UIColor *)color;

@end


@interface UIView (GTUIViewAdditions_nib)

/**
 *  load view from nib (nib name is the same as class name)
 *
 *  @return UIView
 */
+ (id)gt_viewFromNib;

/**
 *  load view from nib
 *
 *  @param nibName xib file name
 *
 *  @return UIView
 */
+ (id)gt_viewFromNib:(NSString *)nibName;

/**
 *  get UINib from nib, mainly used for UITableView / UICollectionView (nib name is the same as class name)
 *
 *  @return UINib
 */
+ (UINib *)gt_uinib;

/**
 *  get UINib from nib, mainly used for UITableView / UICollectionView
 *
 *  @param nibName xib file name
 *
 *  @return UINib
 */
+ (UINib *)gt_uinibWithName:(NSString *)nibName;


/**
 *  load view from a UINib object
 *
 *  @param nib
 *
 *  @return UIView
 */
+ (id)gt_viewFromUINib:(UINib *)nib;

@end



@interface UIView (GTUIViewAdditions_appearence)

/**
 *  make corner radius as 50% of height
 */
- (void)gt_addCircleCornerRadius;

/**
 *  set corner radius with given value
 *
 *  @param cornerRadius
 */
- (void)gt_addCornerRadius:(float)cornerRadius;

@end





