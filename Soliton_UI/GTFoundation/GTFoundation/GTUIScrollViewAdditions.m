// Referenece from iOSKit

#import "GTUIScrollViewAdditions.h"


#define kGTFKDefaultPadding 2.f


BOOL gt_FKViewUseForAutocalculation(UIView *view);
CGPoint gt_FKGetMaxPositions(UIScrollView *scrollView);

#pragma mark - UIScrollView+FKContentSize
@implementation UIScrollView (FKContentSize)

- (void)gt_autocalculateContentHeight {
	[self gt_autocalculateContentHeightWithPadding:kGTFKDefaultPadding];
}

- (void)gt_autocalculateContentHeightWithPadding:(CGFloat)padding {
	CGFloat maxHeight = gt_FKGetMaxPositions(self).y;
    
    self.contentSize = CGSizeMake(self.bounds.size.width, maxHeight + padding);
}

- (void)gt_autocalculateContentWidth {
    [self gt_autocalculateContentWidthWithPadding:kGTFKDefaultPadding];
}

- (void)gt_autocalculateContentWidthWithPadding:(CGFloat)padding {
    CGFloat maxWidth = gt_FKGetMaxPositions(self).x;
    
    self.contentSize = CGSizeMake(maxWidth + padding, self.bounds.size.height);
}

- (void)gt_autocalculateContentSize {
    [self gt_autocalculateContentSizeWithPadding:CGSizeMake(kGTFKDefaultPadding, kGTFKDefaultPadding)];
}

- (void)gt_autocalculateContentSizeWithPadding:(CGSize)padding {
    CGPoint maxPositions = gt_FKGetMaxPositions(self);
    
    self.contentSize = CGSizeMake(maxPositions.x+padding.width, maxPositions.y+padding.height); 
}

@end

////////////////////////////////////////////////////////////////////////
#pragma mark - Helper Functions
////////////////////////////////////////////////////////////////////////

BOOL gt_FKViewIsScrollIndicator(UIView *view) {
    // TODO: Is there a better way to detect the scrollIndicators?
    if ([view isKindOfClass:[UIImageView class]]) {
        if (CGRectGetHeight(view.frame) == 7.f || CGRectGetWidth(view.frame) == 7.f) {
            id image = [view performSelector:@selector(image)];
            
            if ([image isKindOfClass:NSClassFromString(@"_UIResizableImage")]) {
                return YES;
            }
        }
    }
    
    return NO;
}

BOOL gt_FKViewUseForAutocalculation(UIView *view) {
    return view.alpha > 0.f && view.hidden == NO && !gt_FKViewIsScrollIndicator(view);
}

CGPoint gt_FKGetMaxPositions(UIScrollView *scrollView) {
    NSArray *subviews = scrollView.subviews;
	CGPoint max = CGPointZero;
    
    // calculate max position of any subview of the scrollView
	for (UIView *view in subviews) {
        if (gt_FKViewUseForAutocalculation(view)) {
            CGFloat maxXOfView = CGRectGetMaxX(view.frame);
            CGFloat maxYOfView = CGRectGetMaxY(view.frame);
            
            max.x = MAX(max.x, maxXOfView);
            max.y = MAX(max.y, maxYOfView);
        }
	}
    
    return max;
}