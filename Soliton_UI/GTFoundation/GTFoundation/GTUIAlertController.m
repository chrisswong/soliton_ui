//
//  GTUIAlertController.m
//  GTFoundation
//
//  Created by Wai Cheung on 10/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTUIAlertController.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UIAlertAction(Private)
@property (nonatomic) NSInteger buttonIndex;
@end

static char kUIAlertActionButtonIndexKey;

@implementation UIAlertAction(Private)
- (void)setButtonIndex:(NSInteger)buttonIndex
{
    objc_setAssociatedObject(self, &kUIAlertActionButtonIndexKey, @(buttonIndex), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSInteger)buttonIndex
{
    return [objc_getAssociatedObject(self, &kUIAlertActionButtonIndexKey) integerValue];
}
@end


@implementation GTUIAlertController

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSArray *)otherTitles
                     userInfo:(NSDictionary *)userInfo
                 useAlertView:(BOOL)useAlertView
                   completion:(void(^)(id alert, NSDictionary *userInfo, BOOL isCancelled, NSInteger buttonIndex))completion {
    
    if ((self = [super init])) {
        
        self.completionBlock = completion;
        self.userInfo = userInfo;
        
        if (NSClassFromString(@"UIAlertController") && !useAlertView) {
            
            // use iOS8 `UIAlertController`
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
            
            __weak typeof(self)weakSelf = self;
            
            if (cancelButtonTitle.length) {
                
                UIAlertAction *action = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    if (weakSelf.completionBlock) {
                        weakSelf.completionBlock(weakSelf.alert, weakSelf.userInfo, YES, action.buttonIndex);
                    }
                }];
                
                action.buttonIndex = 0;
                [alert addAction:action];
            }
            
            if ([otherTitles isKindOfClass:[NSArray class]] && otherTitles.count) {
                
                NSInteger btnIdx = (cancelButtonTitle.length) ? 1 : 0;
                for (NSString *aTitle in otherTitles) {
                    
                    UIAlertAction *action = [UIAlertAction actionWithTitle:aTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        if (weakSelf.completionBlock) {
                            weakSelf.completionBlock(weakSelf.alert, weakSelf.userInfo, NO, action.buttonIndex);
                        }
                    }];
                    
                    action.buttonIndex = btnIdx;
                    [alert addAction:action];
                    
                    btnIdx++;
                }
            }
            
            _alert = alert;
        }
        else {
            
            // use `UIAlertView`
            UIAlertView *alert = [[UIAlertView alloc] init];
            alert.title = title;
            alert.message = message;
            
            if (cancelButtonTitle.length) {
                [alert addButtonWithTitle:cancelButtonTitle];
                [alert setCancelButtonIndex:0];
            }
            
            if ([otherTitles isKindOfClass:[NSArray class]] && otherTitles.count) {
                for (NSString *aTitle in otherTitles) {
                    [alert addButtonWithTitle:aTitle];
                }
            }
            
            alert.delegate = self;
            _alert = alert;
        }
    }
    return self;
    
}

- (void)show {
    [self showAtViewController:nil];
}

- (void)showAtViewController:(UIViewController *)viewController{
    if ([_alert isKindOfClass:NSClassFromString(@"UIAlertController")]) {
        UIAlertController *ac = _alert;
        if (viewController) {
            [viewController presentViewController:ac animated:YES completion:nil];
        } else {
            if ([[UIApplication sharedApplication] windows].count) {
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:ac animated:YES completion:nil];
            }
        }
    } else {
        [(UIAlertView *)_alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (self.completionBlock) {
        self.completionBlock(alertView, self.userInfo, (buttonIndex == alertView.cancelButtonIndex), buttonIndex);
    }
}

@end
