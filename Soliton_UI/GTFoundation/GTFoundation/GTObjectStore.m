//
//  GTObjectStore.m
//  GTWebAppBridgeDemo
//
//  Created by Wai Cheung on 26/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTObjectStore.h"
#include "GTCommon.h"

static NSString * kDefaultStoreName = @"GTObjectStore";

@implementation GTObjectStore

+ (instancetype)sharedStore {
    static GTObjectStore *_objectStore;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _objectStore = [[GTObjectStore alloc] initWithName:kDefaultStoreName];
    });
    return _objectStore;
}

- (instancetype)initWithName:(NSString *)name {
    return [self initWithName:name appGroupId:nil];
}

- (instancetype)initWithName:(NSString *)name appGroupId:(NSString *)appGroupId {
    if ((self=[super init])) {
        NSAssert((name && name.length), @"name cannot be empty or null");
        _name = name;
        _appGroupId = appGroupId;
    }
    return self;
}

- (void)setObject:(id)obj forKey:(id<NSCopying>)aKey {
    [self setObject:obj forKeyedSubscript:aKey];
}

- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key {
    if ([obj isKindOfClass:[NSObject class]] && ([(NSObject *)key isKindOfClass:[NSString class]] || [(NSObject *)key isKindOfClass:[NSNumber class]])) {
        NSString *newKey = [self _stringfyWithKey:key];
        NSAssert(newKey!=nil, @"new key cannot be nil");
        
        NSUserDefaults *userDefault = [self _storeUserDefault];
        [userDefault setObject:[NSKeyedArchiver archivedDataWithRootObject:obj] forKey:[self _storeCombinedKey:newKey]];
        [userDefault synchronize];
        
        [self _addNewKeyToList:[self _storeCombinedKey:newKey]];
    } else {
        GTLog(@"object is not supported.");
    }
}

- (id)objectForKey:(id<NSCopying>)aKey {
    return [self objectForKeyedSubscript:aKey];
}

- (id)objectForKeyedSubscript:(id <NSCopying>)key {
    NSString *newKey = [self _stringfyWithKey:key];
    NSAssert(newKey!=nil, @"new key cannot be nil");
    
    NSUserDefaults *userDefault = [self _storeUserDefault];
    NSData *data = [userDefault objectForKey:[self _storeCombinedKey:newKey]];
    if (data) {
        return [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    return data;
}

- (void)removeObjectForKey:(id<NSCopying>)aKey {
    NSString *newKey = [self _stringfyWithKey:aKey];
    NSAssert(newKey!=nil, @"new key cannot be nil");
    
    NSUserDefaults *userDefault = [self _storeUserDefault];
    [userDefault setObject:nil forKey:[self _storeCombinedKey:newKey]];
    [self _removeKeyFromList:[self _storeCombinedKey:newKey]];
    [userDefault synchronize];
}

- (void)removeAllObjects {
    NSUserDefaults *userDefault = [self _storeUserDefault];
    
    NSMutableArray *mStoreKeysList = [[self _storedKeysList] mutableCopy];
    for (NSString *key in mStoreKeysList) {
        [userDefault setObject:nil forKey:key];
    }
    
    NSString *keysListName = [self _objectStoreKeysListName];
    [userDefault setObject:nil forKey:keysListName];
    [userDefault synchronize];
}

#pragma mark - Private
////////////////////////////////////////////////////////////////////////////
- (NSString *)_stringfyWithKey:(id<NSCopying>)aKey {
    NSString *newKey = nil;
    if ([(NSObject *)aKey isKindOfClass:[NSNumber class]]) {
        newKey = [(NSNumber *)aKey stringValue];
    } else if ([(NSObject *)aKey isKindOfClass:[NSString class]]){
        newKey = (NSString *)aKey;
    }
    return newKey;
}

- (NSUserDefaults *)_storeUserDefault {
    if (_appGroupId) {
        return [[NSUserDefaults alloc] initWithSuiteName:_appGroupId];
    }
    return [NSUserDefaults standardUserDefaults];
}

- (NSString *)_storeCombinedKey:(NSString *)key {
    return [NSString stringWithFormat:@"%@.%@", _name, (NSString *)key];
}

- (NSString *)_objectStoreKeysListName {
    return [NSString stringWithFormat:@"GT_OBJECT_STORE_LIST_%@",_name];
}

- (NSArray *)_storedKeysList {
    NSUserDefaults *userDefault = [self _storeUserDefault];
    
    NSString *keysListName = [self _objectStoreKeysListName];
    NSData *data = [userDefault objectForKey:keysListName];
    NSArray *keysList;
    if (data) {
        keysList = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    else {
        // create list
        keysList = @[];
        
        
        [userDefault setObject:[NSKeyedArchiver archivedDataWithRootObject:keysList] forKey:keysListName];
        [userDefault synchronize];
    }
    return keysList;
}

- (void)_addNewKeyToList:(NSString *)newKey {
    NSMutableArray *mStoreKeysList = [[self _storedKeysList] mutableCopy];
    
    if (![mStoreKeysList containsObject:newKey]) {
        [mStoreKeysList addObject:newKey];
        
        NSString *keysListName = [self _objectStoreKeysListName];
        
        NSUserDefaults *userDefault = [self _storeUserDefault];
        [userDefault setObject:[NSKeyedArchiver archivedDataWithRootObject:mStoreKeysList] forKey:keysListName];
        [userDefault synchronize];
    }
}

- (void)_removeKeyFromList:(NSString *)key {
    NSMutableArray *mStoreKeysList = [[self _storedKeysList] mutableCopy];
    [mStoreKeysList removeObject:key];
    
    NSString *keysListName = [self _objectStoreKeysListName];
    
    NSUserDefaults *userDefault = [self _storeUserDefault];
    [userDefault setObject:[NSKeyedArchiver archivedDataWithRootObject:mStoreKeysList] forKey:keysListName];
    [userDefault synchronize];
}


@end
