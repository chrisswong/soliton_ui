//
//  GTNSStringAdditions.m
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTNSStringAdditions.h"
#import "GTCommon.h"

@implementation NSString (GTNSStringAdditions)

- (NSData *)gt_toData {
    return [self dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSURL *)gt_toURL {
    return [NSURL URLWithString:self];
}

- (NSURL *)gt_toFileURL {
    return [NSURL fileURLWithPath:self];
}

- (BOOL)gt_isValidEmail {
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[a-z0-9_+-.]+@([a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?\\.)+([a-z0-9]([a-z0-9-]{0,61}[a-z0-9])?)$";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

+ (NSString *)gt_queryFromDictionary:(NSDictionary *)dictionary {
    NSMutableArray* pairs = [NSMutableArray array];
    // loop all entries
    for (NSString* key in [dictionary keyEnumerator]) {
        
        // extract value
        id value = dictionary[key];
        
        if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]] || [value isKindOfClass:[NSArray class]]) {
            
            // handle array make it as csv
            if ([value isKindOfClass:[NSArray class]]) {
                NSMutableArray *subsetArray = [NSMutableArray array];
                for (id inValue in value) {
                    // only allow string or number
                    if ([inValue isKindOfClass:[NSString class]] || [inValue isKindOfClass:[NSNumber class]]) {
                        NSString *addObject;
                        if ([inValue isKindOfClass:[NSNumber class]]) {
                            addObject = [(NSNumber *)inValue stringValue];
                        } else {
                            addObject = inValue;
                        }
                        [subsetArray addObject:addObject];
                    }
                }
                // join with comma
                value = [(NSArray *)value componentsJoinedByString:@","];
                
            }
            else if ([value isKindOfClass:[NSNumber class]]) {
                
                value = [value stringValue];
            }
            
            CFStringRef string = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                         (__bridge CFStringRef)value,
                                                                         NULL,
                                                                         CFSTR(":/?#[]@!$&’()*+,;="),
                                                                         kCFStringEncodingUTF8 );
            
            NSString * encodedString = (__bridge_transfer NSString *)string;
            NSString* pair = [NSString stringWithFormat:@"%@=%@", key, encodedString];
            [pairs addObject:pair];

        }
    }
    
    NSString* params = [pairs componentsJoinedByString:@"&"];
    
    return params;
}

- (NSString*)gt_stringByAddingQuery:(NSDictionary*)query {
    
    if ([self rangeOfString:@"?"].location == NSNotFound) {
        
        return [self stringByAppendingFormat:@"?%@", [[self class] gt_queryFromDictionary:query]];
    }
    else {
        return [self stringByAppendingFormat:@"&%@", [[self class] gt_queryFromDictionary:query]];
    }
}

- (NSComparisonResult)gt_versionStringCompare:(NSString *)other {
    return [self compare:other options:NSNumericSearch];
}

- (NSString *)gt_stringByStrippingHTML
{
    NSRange range;
    NSString *string = [self copy];
    while ((range = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound) {
        string = [string stringByReplacingCharactersInRange:range withString:@""];
    }
    return string;
}

- (BOOL)gt_isEqualIgnoreCaseWithString:(NSString *)string {
    return [[self lowercaseString] isEqualToString:[string lowercaseString]];
}

@end

@implementation NSData (GTNSStringAdditions)

- (NSString *)gt_toString {
    return [[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
}

- (NSString *)gt_toStringWithEncoding:(NSStringEncoding)encoding {
    return [[NSString alloc] initWithData:self encoding:encoding];
}

@end

/**
 *  JSON relate
 */
@implementation NSData (GTNSStringAdditions_JSON)

- (id)gt_JSONObject {
    NSError *error;
    id result = [NSJSONSerialization JSONObjectWithData:self options:0 error:&error];
    if (error) {
        GTLog(@"%@",error);
    }
    return result;
}

- (id)gt_JSONMutableObject {
    NSError *error;
    id result = [NSJSONSerialization JSONObjectWithData:self options:NSJSONReadingMutableContainers error:&error];
    if (error) {
        GTLog(@"%@",error);
    }
    return result;
}

@end


@implementation NSString (GTNSStringAdditions_JSON)

- (id)gt_JSONObject {
    NSData *jsonData = [self dataUsingEncoding:NSUTF8StringEncoding];
    return [jsonData gt_JSONObject];
}

- (id)gt_JSONMutableObject {
    NSData *jsonData = [self dataUsingEncoding:NSUTF8StringEncoding];
    return [jsonData gt_JSONMutableObject];
}

@end

@implementation NSDictionary (GTNSStringAdditions_JSON)

- (NSData *)gt_toJSONData {
    NSError *error;
    NSData *result =[NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
    if (error) {
        GTLog(@"%@",error);
    }
    return result;
}

- (NSString *)gt_toJSONString {
    NSData *result = [self gt_toJSONData];
    return [result gt_toString];
}


@end


@implementation NSArray (GTNSStringAdditions_JSON)

- (NSData *)gt_toJSONData {
    NSError *error;
    NSData *result =[NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
    if (error) {
        GTLog(@"%@",error);
    }
    return result;
}

- (NSString *)gt_toJSONString {
    NSData *result = [self gt_toJSONData];
    return [result gt_toString];
}

@end

/**
 *  Hash
 */
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (GTFoundation_Hashes)

- (NSString *)gt_md5
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

- (NSString *)gt_sha1
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

- (NSString *)gt_sha256
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

- (NSString *)gt_sha512
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    
    CC_SHA512(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}

@end
