//
//  GTCommon.h
//  GTFoundation
//  
//  Copyright (c) 2013 Green Tomato. All rights reserved.
//

#ifndef GT_COMMON_H
#define GT_COMMON_H

//////////////////////////////////////////////////////////////////
// Debug Support
//////////////////////////////////////////////////////////////////

#ifdef DEBUG
    #define GTLog(...) do{ NSLog(@"%s [Line %d] %@", __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:__VA_ARGS__]); }while(0)
#else
    #define GTLog(...) do{}while(0)
#endif

/* print CG Structs */
#define GTLogLine() GTLog("")
#define GTLogPoint(pt) GTLog(@"%s x=%f, y=%f", #pt, pt.x, pt.y)
#define GTLogRect(rect) GTLog(@"%s x=%f, y=%f, w=%f, h=%f", #rect, rect.origin.x, rect.origin.y, rect.size.width, rect.size.height)
#define GTLogSize(size) GTLog(@"%s w=%f, h=%f", #size, size.width, size.height)
#define GTLogEdges(edges) GTLog(@"%s left=%f, right=%f, top=%f, bottom=%f", #edges, edges.left, edges.right, edges.top, edges.bottom)


//////////////////////////////////////////////////////////////////
// System Versioning Preprocessor Macros
//////////////////////////////////////////////////////////////////
#define GT_SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define GT_SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define GT_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define GT_SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define GT_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


///////////////////////////////////////////////////////////////////////////////////////////////////
// General ShortHand
///////////////////////////////////////////////////////////////////////////////////////////////////
#define GT_FILE_MANAGER             [NSFileManager defaultManager]
#define GT_USER_DEFAULT             [NSUserDefaults standardUserDefaults]
#define GT_NOTIF_CENTER             [NSNotificationCenter defaultCenter]
#define GT_BUNDLE                   [NSBundle mainBundle]
#define GT_DEVICE_MODEL             [[UIDevice currentDevice] model]
#define GT_APP_ID                   [[NSBundle mainBundle] bundleIdentifier]
#define GT_APP_VERSION              [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
#define GT_APP_BUILD_VERSION        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
// only work >= iOS 6
#define GT_APP_VENDOR_UUID          [[[UIDevice currentDevice] identifierForVendor] UUIDString]

#endif