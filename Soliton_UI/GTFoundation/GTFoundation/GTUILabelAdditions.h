//
//  GT_UILabelAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 11/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import Foundation;
@import UIKit;


/**
 *  Get size of string
 *
 *  @param string string
 *  @param font   font
 *
 *  @return CGSize of string
 */
CGSize gt_UIGetSizeFromString(NSString *string, UIFont *font);

/**
 *  Get size of multiple line string
 *
 *  @param string          string
 *  @param font            font
 *  @param constrainedSize constrainedSize
 *
 *  @return CGSize of multiple line string
 */
CGSize gt_UIGetSizeFromMultipleLineString(NSString *string, UIFont *font, CGSize constrainedSize);


@interface UILabel (GTUILabelAdditions_adjust_frame)

/**
 *  adjust size of label to fit with label's font
 */
- (void)gt_adjustsSizeToFitLabelFont;

/**
 *  adjust height of label to fit with label's font and width
 */
- (void)gt_adjustsHeightToFitWidthAndFont;

/**
 *  adjust height of label to fit with label's font, width, maximum height
 */
- (void)gt_adjustsHeightToFitWidthAndFontWithMaxHeight:(float)maxHeight;

/**
 *  adjust height of label to fit with label's font width; label's height changed only when new label's height > current label's height
 */
- (void)gt_adjustsHeightToFitWidthAndFontIfNeeded;

@end
