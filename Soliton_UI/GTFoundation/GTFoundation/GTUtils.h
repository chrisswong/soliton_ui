//
//  GTUtils.h
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import Foundation;
@import UIKit;
@import AudioToolbox;

@interface GTUtils : NSObject

/**
 *  Check if object is null or empty string/array/dictionary
 *
 *  @param object
 *
 *  @return YES if object is null or empty string/array/dictionary; otherwise, return NO;
 */
+ (BOOL)isEmpty:(id)object;

+ (NSString *)emptyStringIfNil:(NSString *)string;

+ (double)radianFromDegree:(double)degree;

@end

@interface GTUtils(TextInput)

/**
 *  textfield input is restricted by max character count, if true, the textfield is allowed to be changed
 *
 *  - (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
 *      return [GTUtils textInput:searchBar allowChangeTextWithMaxCount:50 inRange:range replacementText:text];
 *  }
 *
 *  @param textInput         UITextfield / UITextView / UISearchBar
 *  @param maxCharacterCount max character count can be inserted
 *  @param range             range size from textInput's delegate
 *  @param text              replacement text from textInput's delegate
 *
 *  @return YES if textinput is allowed to be changed; otherwise, return NO
 */
+ (BOOL)textInput:(id)textInput allowChangeTextWithMaxCount:(int)maxCharacterCount inRange:(NSRange)range replacementText:(NSString *)text;


/**
 *  Get keyboard size for keyboard noficiation
 *
 *  @param notification keyboard noficiation
 *
 *  @return CGSize keyboard size
 */
+ (CGSize)getKeyboardSizeFromKeyboardNotification:(NSNotification *)notification;


/**
 *  Get keyboard animation duration for keyboard noficiation
 *
 *  @param notification keyboard noficiation
 *
 *  @return NSTimeInterval keyboard animation duration
 */
+ (NSTimeInterval)getKeyboardAnimationDurationFromKeyboardNotification:(NSNotification *)notification;

@end

@interface GTUtils(LaunchTimes)

/**
 *  NSUserDefault key of launch times count
 */
extern NSString *const kGTUtilsLaunchTimesCountKey;

/**
 *  is first times launch
 *
 *  @return YES if it is first times launch
 */
+ (BOOL)isFirstTimesLaunch;

/**
 *  get latest launch times count
 *
 *  @return latest launch times count
 */
+ (NSInteger)getLaunchCount;

/**
 *  increase launch count by 1
 *
 *  @return latest launch times count
 */
+ (NSInteger)increaseLaunchCount;

@end


typedef NS_ENUM(NSUInteger, GTUtilsVersionCompare) {
    GTUtilsVersionSame,
    GTUtilsVersionGreaterThan,
    GTUtilsVersionGreaterThanOrEqual,
    GTUtilsVersionLessThan,
    GTUtilsVersionLessThanOrEqual,
};

@interface GTUtils(Device)

/**
 *  make device vibrate
 */
+ (void)deviceVibrate;

/**
 *  system version compare
 *
 *  @param compareOperator
 *  @param targerSystemVerion
 *
 *  @return YES if condition matched
 */
+ (BOOL)isSystemVersion:(GTUtilsVersionCompare)compareOperator toVersion:(NSString *)targerSystemVerion;

/**
 *  check device ui is iPhone-like
 *
 *  @return YES if ui is iPhone-like
 */
+ (BOOL)isIPhoneUI;

/**
 *  check device ui is iPad-like
 *
 *  @return YES if ui is iPhone-like
 */
+ (BOOL)isIPadUI;

/**
 *  check device is iPhone
 *
 *  @return YES if device is iPhone
 */
+ (BOOL)isIPhoneDevice;

/**
 *  check device is iPod
 *
 *  @return YES if device is iPod
 */
+ (BOOL)isIPodDevice;

/**
 *  check device is iPad
 *
 *  @return YES if device is iPad
 */
+ (BOOL)isIPadDevice;

/**
 *  check device has retina display
 *
 *  @return YES if device has retina display
 */
+ (BOOL)isRetinaDisplay;

/**
 *  convert push token (NSData) to string
 *
 *  @param tokenData NSData
 *
 *  @return stringify push token
 */
+ (NSString *)getTokenFromDeviceTokenData:(NSData *)tokenData;



@end

