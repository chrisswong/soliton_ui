//
//  GTUtils.m
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTUtils.h"
#import "GTCommon.h"

@implementation GTUtils

+ (BOOL)isEmpty:(id)object {
    
    if (!object || [object isEqual:[NSNull null]]) return YES;
    
    if ([object isKindOfClass:[NSString class]] && [(NSString *)object length] == 0) return YES;
    
    if (([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]]) && [(NSArray *)object count] == 0) return YES;
    
    return NO;
}

+ (NSString *)emptyStringIfNil:(NSString *)string {
    if (string == nil) {
        return @"";
    }
    return string;
}


+ (double)radianFromDegree:(double)degree {
    return M_PI/180*degree;
}



@end

@implementation GTUtils(TextInput)

+ (BOOL)textInput:(id)textInput allowChangeTextWithMaxCount:(int)maxCharacterCount inRange:(NSRange)range replacementText:(NSString *)text {
    
    NSAssert([textInput respondsToSelector:@selector(text)], @"textInput is not response to @selector(text)");
    NSString *inputText = [textInput text];
    NSUInteger newLength = (inputText.length - range.length) + text.length;
    if(newLength <= maxCharacterCount){
        return YES;
    } else {
        NSUInteger emptySpace = maxCharacterCount - (inputText.length - range.length);
        [textInput setText:[[[inputText substringToIndex:range.location]
                             stringByAppendingString:[text substringToIndex:emptySpace]]
                            stringByAppendingString:[inputText substringFromIndex:(range.location + range.length)]]];
        return NO;
    }
}

+ (CGSize)getKeyboardSizeFromKeyboardNotification:(NSNotification *)notification {
    return [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
}

+ (NSTimeInterval)getKeyboardAnimationDurationFromKeyboardNotification:(NSNotification *)notification {
    NSTimeInterval animationDuration;
    [(NSValue*)[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    return animationDuration;
}

@end

@implementation GTUtils(LaunchTimes)

NSString *const kGTUtilsLaunchTimesCountKey = @"kGTUtilsLaunchTimesCountKey";

+ (BOOL)isFirstTimesLaunch {
    NSNumber *launchCountNum = [[NSUserDefaults standardUserDefaults] objectForKey:kGTUtilsLaunchTimesCountKey];
    return (!launchCountNum);
}


+ (NSInteger)getLaunchCount {
    NSNumber *launchCountNum = [[NSUserDefaults standardUserDefaults] objectForKey:kGTUtilsLaunchTimesCountKey];
    // if launchCountNum is nil, set launch count as zero
    NSInteger launchCount = (launchCountNum) ? [launchCountNum integerValue] : 0;
    return launchCount;
}

+ (NSInteger)increaseLaunchCount {
    
    NSInteger launchCount = [[self class] getLaunchCount];
    
    //increase launch count by 1
    launchCount += 1;
    
    // update count
    [[NSUserDefaults standardUserDefaults] setObject:@(launchCount) forKey:kGTUtilsLaunchTimesCountKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return launchCount;
}

@end


@implementation GTUtils(Device)

+ (void)deviceVibrate {
    AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
    
    // Get the main bundle for the app
    CFBundleRef mainBundle;
    SystemSoundID soundFileObject;
    mainBundle = CFBundleGetMainBundle();
    
    // Get the URL to the sound file to play
    CFURLRef soundFileURLRef  = CFBundleCopyResourceURL (mainBundle,CFSTR("do"),CFSTR("wav"),NULL);
    // Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (soundFileURLRef,&soundFileObject);
    // Play the audio
    AudioServicesPlaySystemSound(soundFileObject);
    
    CFRelease(soundFileURLRef);
}

+ (BOOL)isSystemVersion:(GTUtilsVersionCompare)compareOperator toVersion:(NSString *)targerSystemVerion {
    switch (compareOperator) {
        case GTUtilsVersionSame:
            return GT_SYSTEM_VERSION_EQUAL_TO(targerSystemVerion);
            
        case GTUtilsVersionGreaterThan:
            return GT_SYSTEM_VERSION_GREATER_THAN(targerSystemVerion);
            
        case GTUtilsVersionGreaterThanOrEqual:
            return GT_SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(targerSystemVerion);
            
        case GTUtilsVersionLessThan:
            return GT_SYSTEM_VERSION_LESS_THAN(targerSystemVerion);
            
        case GTUtilsVersionLessThanOrEqual:
            return GT_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(targerSystemVerion);
            
        default:
            return NO;
    }
    return NO;
}

+ (BOOL)isIPhoneUI {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

+ (BOOL)isIPadUI {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
}

+ (BOOL)isIPhoneDevice {
    return ([[UIDevice currentDevice].model rangeOfString:@"iPhone" options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location != NSNotFound);
}

+ (BOOL)isIPodDevice {
    return ([[UIDevice currentDevice].model rangeOfString:@"iPod" options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location != NSNotFound);
}

+ (BOOL)isIPadDevice {
    return ([[UIDevice currentDevice].model rangeOfString:@"iPad" options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location != NSNotFound);
}

+ (BOOL)isRetinaDisplay {
    return ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2);
}


+ (NSString *)getTokenFromDeviceTokenData:(NSData *)tokenData {
    return [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
            ntohl(((const unsigned *)[tokenData bytes])[0]),
            ntohl(((const unsigned *)[tokenData bytes])[1]),
            ntohl(((const unsigned *)[tokenData bytes])[2]),
            ntohl(((const unsigned *)[tokenData bytes])[3]),
            ntohl(((const unsigned *)[tokenData bytes])[4]),
            ntohl(((const unsigned *)[tokenData bytes])[5]),
            ntohl(((const unsigned *)[tokenData bytes])[6]),
            ntohl(((const unsigned *)[tokenData bytes])[7])];
}

@end