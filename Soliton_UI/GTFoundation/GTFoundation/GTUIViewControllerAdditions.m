//
//  GTUIViewControllerAdditions.m
//  GTFoundation
//
//  Created by Cheung Chun Wai on 22/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTUIViewControllerAdditions.h"

@implementation UIViewController (GTUIViewControllerAdditions)

+ (instancetype)gt_viewControllerFromNib{
    return [[self class] gt_viewControllerFromNibWithSuffix:@""];
}

+ (instancetype)gt_viewControllerFromNibWithSuffix:(NSString *)suffix{
    return [[[self class] alloc] initWithNibName:[NSString stringWithFormat:@"%@%@",NSStringFromClass([self class]), suffix] bundle:nil];
}

- (NSInteger)gt_currentViewControllerIndex
{
    if (self.navigationController) {
        return [self.navigationController.viewControllers indexOfObject:self];
    }
    return NSNotFound;
}

- (UIViewController*)gt_previousViewController
{
    if (self.navigationController && self.gt_currentViewControllerIndex != NSNotFound && self.gt_currentViewControllerIndex>0) {
        return [self.navigationController.viewControllers objectAtIndex:self.gt_currentViewControllerIndex-1];
    }
    return nil;
}

- (UIView *)gt_enumerateAllSubviewsOf:(UIView*)aView usingBlock:(BOOL (^)( UIView* aView ))aBlock {
    for ( UIView* aSubView in aView.subviews ) {
        if (aBlock(aSubView)) {
            return aSubView;
        } else if (![ aSubView isKindOfClass:[UIControl class]]){
            UIView* result = [self gt_enumerateAllSubviewsOf:aSubView usingBlock:aBlock];
            if (result != nil) {
                return result;
            }
        }
    }
    return nil;
}

- (UIView*)gt_enumerateAllSubviewsUsingBlock: (BOOL (^)( UIView* aView )) aBlock {
    return [self gt_enumerateAllSubviewsOf: self.view usingBlock: aBlock ];
}

- (UIView*)gt_findFirstResponder {
    return [self gt_enumerateAllSubviewsUsingBlock:^BOOL(UIView *aView) {
        if ([aView isFirstResponder]) {
            return YES;
        }
        return NO;
    }];
}

- (void)gt_addChildViewController:(UIViewController *)childController toView:(UIView *)view {
    [self addChildViewController:childController];
    [view addSubview:childController.view];
    [childController didMoveToParentViewController:self];
}

- (void)gt_addChildViewController:(UIViewController *)childController toView:(UIView *)view updateChildControllerLayout:(void(^)(UIView *childView, UIView *toView))updateChildControllerLayoutBlock{
    [self addChildViewController:childController];
    if (updateChildControllerLayoutBlock) {
        updateChildControllerLayoutBlock(childController.view, view);
    }
    [view addSubview:childController.view];
    [childController didMoveToParentViewController:self];
    
}

- (void)gt_removeFromSuperViewController {
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
