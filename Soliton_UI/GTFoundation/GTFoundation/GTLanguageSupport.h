//
//  GTLanguageSupport.h
//  GTFoundation
//	
//  Copyright (c) 2013 Green Tomato. All rights reserved.
//

@import UIKit;

// Define language that will be use
#define GT_LANG_ZH @"tc"   // Tranditional Chinese
#define GT_LANG_EN @"en"   // English
#define GT_LANG_SC @"sc"   // Simplified Chinese

#ifndef kGTLanguageDidChangeNotification
#define kGTLanguageDidChangeNotification @"kGTLanguageDidChangeNotification"
#endif

// Return application language.
NSString* gt_appLanguage(void);

// Set application language.
void gt_setAppLanguage(NSString *lang);

// Return YES if the appLanguage is Equal to <lang>
BOOL gt_isAppLanguage(NSString *lang);

// Return the translated string, based on the appLanguage.
NSString* gt_localizedString(NSString* string);
