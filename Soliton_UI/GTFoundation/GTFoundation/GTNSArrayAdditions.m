//
//  GTNSArrayAdditions.m
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTNSArrayAdditions.h"

@implementation NSArray (GTNSArrayAdditions)

- (NSArray *)gt_shuffledArray {
    NSMutableArray *shuffledArray = [self mutableCopy];
    NSUInteger arrayCount = (u_int32_t)[shuffledArray count];
    
    for (NSUInteger i = arrayCount - 1; i > 0; i--) {
        NSUInteger n = arc4random_uniform((u_int32_t)i + 1);
        [shuffledArray exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    return [shuffledArray copy];
}

- (NSArray *)gt_shuffledArrayWithItemLimit:(NSUInteger)itemLimit {
    if (!itemLimit) return [self gt_shuffledArray];
    
    NSMutableArray *shuffledArray = [self mutableCopy];
    NSUInteger arrayCount = (u_int32_t)[shuffledArray count];
    
    NSUInteger loopCounter = 0;
    for (NSUInteger i = arrayCount - 1; i > 0 && loopCounter < itemLimit; i--) {
        NSUInteger n = arc4random_uniform((u_int32_t)i + 1);
        [shuffledArray exchangeObjectAtIndex:i withObjectAtIndex:n];
        loopCounter++;
    }
    
    NSArray *arrayWithLimit;
    if (arrayCount > itemLimit) {
        NSRange arraySlice = NSMakeRange(arrayCount - loopCounter, loopCounter);
        arrayWithLimit = [shuffledArray subarrayWithRange:arraySlice];
    } else
        arrayWithLimit = [shuffledArray copy];
    
    return arrayWithLimit;
}

@end
