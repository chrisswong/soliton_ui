//
//  GTUIColorAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

@import UIKit;

// shorthand
// e.g. GT_COLOR_HEX(@"EFEFEF", 1.0)
#define GT_COLOR_HEX(__hex, __alpha) [UIColor gt_colorWithHexString:__hex alpha:__alpha]


@interface UIColor (GTUIColorAdditions)

/**
 *  Create a new UIColor instance using the following long value (see
 *  UIColor#colorWithHex for an example) and desired alpha value.
 *  1.0 for opaque, 0.0 for transparent.
 *  
 *  Example: Light gray color of EFEFEF 
 *  [UIColor gt_colorWithHex:0xEFEFEF alpha:1.0]
 *
 *  @param hexColor
 *  @param alpha
 *
 *  @return UIColor
 */
+ (UIColor *)gt_colorWithHex:(long)hexColor alpha:(CGFloat)alpha;

/**
 *  convert hex string to color
 *
 *  @param stringToConvert hex string
 *  @param alpha
 *
 *  @return UIColor
 */
+ (UIColor *)gt_colorWithHexString:(NSString *)stringToConvert alpha:(CGFloat)alpha;

@end
