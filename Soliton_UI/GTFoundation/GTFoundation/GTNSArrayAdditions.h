//
//  GTNSArrayAdditions.h
//  GTFoundation
//
//  Created by Wai Cheung on 13/9/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (GTNSArrayAdditions)

/**
 *  shuffle array in random order
 *
 *  @return NSArray
 */
- (NSArray *)gt_shuffledArray;

/**
 *  return new shuffled array with limited size
 *
 *  @param itemLimit limited item size
 *
 *  @return NSArray
 */
- (NSArray *)gt_shuffledArrayWithItemLimit:(NSUInteger)itemLimit;

@end
