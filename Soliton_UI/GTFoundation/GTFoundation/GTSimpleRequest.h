//
//  GTSimpleRequest.h
//
//  Created by Wai Cheung on 27/12/13.
//  Copyright (c) 2013 Green Tomato Limited. All rights reserved.
//

@import Foundation;

@interface GTSimpleRequest : NSObject

/**
 *  http response
 */
@property (strong, nonatomic, readonly) NSHTTPURLResponse *response;

/**
 *  url connection object
 */
@property (strong, nonatomic, readonly) NSURLConnection *connection;

/**
 *  response cookies
 */
@property (strong, nonatomic, readonly) NSDictionary *cookies;

/**
 *  response error
 */
@property (strong, nonatomic, readonly) NSError *error;

/**
 *  response reponse data
 */
@property (strong, nonatomic, readonly) NSData *responseData;

/**
 *  response response stirng (utf8)
 */
@property (strong, nonatomic, readonly) NSString *responseString;

/**
 *  request url
 */
@property (copy, nonatomic, readonly) NSString *urlString;

/**
 *  http method
 */
@property (copy, nonatomic, readonly) NSString *HTTPMethod;

/**
 *  request parameters
 */
@property (copy, nonatomic, readonly) NSDictionary *params;

/**
 *  success block
 */
@property (copy, nonatomic) void(^successBlock)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data);

/**
 *  fail block
 */
@property (copy, nonatomic) void(^failureBlock)(NSError *err);

/**
 *  is running or not
 */
@property (nonatomic, readonly) BOOL isRunning;

/**
 *  time out
 */
@property (nonatomic) NSTimeInterval timeoutInterval;

/**
 *  enable logger
 */
@property (nonatomic) BOOL isEnableLogger;

/**
 *  progress
 */
@property (nonatomic, readonly) double progressValue;

/**
 *  progress callback block
 */
@property (copy, nonatomic) void(^progressBlock)(GTSimpleRequest *request, double percentage);

#pragma mark - member functions
////////////////////////////////////////////////////////////////////////////

/**
 *  GET request
 *
 *  @param urlString       url
 *  @param params          query param
 *  @param requestModifier modify block
 *  @param success         success block
 *  @param failure         fail block
 *
 *  @return GTSimpleRequest
 */
+ (GTSimpleRequest *)GET:(NSString *)urlString
                  params:(NSDictionary *)params
         requestModifier:(void(^)(NSMutableURLRequest *request))requestModifier
                 success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                 failure:(void(^)(NSError *err))failure;

/**
 *  POST request
 *
 *  @param urlString       url
 *  @param params          query param
 *  @param requestModifier modify block
 *  @param success         success block
 *  @param failure         fail block
 *
 *  @return GTSimpleRequest
 */
+ (GTSimpleRequest *)POST:(NSString *)urlString
                   params:(NSDictionary *)params
          requestModifier:(void(^)(NSMutableURLRequest *request))requestModifier
                  success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                  failure:(void(^)(NSError *err))failure;

/**
 *  initialize requeset
 *
 *  @param urlString        url
 *  @param HTTPMethod       http methond (GET/POST)
 *  @param params           query param
 *  @param requestModifier  modify block
 *  @param success          success block
 *  @param failure          fail block
 *
 *  @return GTSimpleRequest
 */
- (instancetype)initWithURLString:(NSString *)urlString
                       HTTPMethod:(NSString *)HTTPMethod
                           params:(NSDictionary *)params
                  requestModifier:(void(^)(NSMutableURLRequest *request))requestModifier
                          success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                          failure:(void(^)(NSError *err))failure;

/**
 *  start request
 */
- (void)startRequest;

/**
 *  cancel request
 */
- (void)cancelRequest;

#pragma mark - class functions
////////////////////////////////////////////////////////////////////////////
// global settings
+ (GTSimpleRequest *)proxy;
+ (NSString *)stringFromCookies:(NSDictionary *)cookies;
+ (NSDictionary *)cookiesFromString:(NSString *)cookiesStr;

@end

@interface GTSimpleRequestAttachment : NSObject
@property (nonatomic, copy) NSString *fileName;
@property (nonatomic, copy) NSData *fileData;
- (instancetype)initWithFileURL:(NSURL *)fileURL;
@end

@interface NSString(GTSimpleRequest_shorthand)

- (GTSimpleRequest *)gt_GETWithParams:(NSDictionary *)params
                              success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                              failure:(void(^)(NSError *err))failure;

- (GTSimpleRequest *)gt_POSTWithParams:(NSDictionary *)params
                               success:(void(^)(NSHTTPURLResponse *res, NSDictionary *cookies, NSData *data))success
                               failure:(void(^)(NSError *err))failure;

@end
