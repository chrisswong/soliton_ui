//
//  GTCache.m
//  GTFoundation
//
//  Created by Wai Cheung on 25/11/14.
//  Copyright (c) 2014 Green Tomato Limited. All rights reserved.
//

#import "GTCache.h"
#import <CommonCrypto/CommonDigest.h>
static NSString *const kCacheSubfolder = @"GTCache";

@interface NSDate(GTCacheAdditions)
- (NSTimeInterval)timeBetweenDate:(NSDate *)date;
@end
@implementation NSDate(CWDataStorageDateAdditions)
- (NSTimeInterval)timeBetweenDate:(NSDate *)date{
    NSTimeInterval time = [self timeIntervalSinceDate:date];
    return fabs(time);
}
@end

@interface GTCache()
@property (strong, nonatomic) NSCache *cache;
@property (copy, nonatomic) NSString *cachePath;
- (NSString *)internalKey:(NSString*)key;
- (NSString *)cachePath;
- (void)createCachePath;
@end

@implementation GTCache

+ (GTCache *)sharedCache {
    static GTCache *_sharedCache;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCache = [[GTCache alloc] init];
    });
    return _sharedCache;
}

- (instancetype)init
{
    if ((self = [super init])) {
        _cache = [[NSCache alloc] init];
    }
    return self;
}

- (BOOL)isDataForKey:(NSString *)key expiredForTime:(NSTimeInterval)second
{
    if(![self isCached:key]) return YES;//not yet cached
    
    NSDate *storedDate = nil;
    NSError *attributesRetrievalError = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [self pathForDataFile:key];
    NSDictionary *attributes = [fileManager attributesOfItemAtPath:filePath
                                                             error:&attributesRetrievalError];
    if (!attributes) {
        return YES;
    }
    if ([attributes fileModificationDate]) {
        storedDate = [attributes fileModificationDate];
    }else{
        storedDate = [attributes fileCreationDate];
    }
    if (!storedDate || [storedDate timeBetweenDate:[NSDate date]]>second) return YES;
    return NO;
}

- (NSString *)cachePath
{
    if (!_cachePath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        _cachePath = [[paths objectAtIndex:0] stringByAppendingFormat:kCacheSubfolder];
    }
    return _cachePath;
}

- (void)createCachePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachePath = [self cachePath];
    if (![fileManager fileExistsAtPath:cachePath]) {
        [fileManager createDirectoryAtPath:cachePath withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (BOOL)isCached:(NSString *)key
{
    NSFileManager* fileManager=[NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:[self pathForDataFile:key]]){
        return YES;
    }
    return NO;
}

- (NSString*)pathForDataFile:(NSString *)key
{
    NSString* internalKey = [self internalKey:key];
    return [[self cachePath] stringByAppendingPathComponent:internalKey];
}

- (void)storeData:(NSData*)data key:(NSString*)key
{
    if (![self isCached:key]){
        [self createCachePath];
        [data writeToFile:[self pathForDataFile:key] atomically:YES];
    }else{
        [self removeCacheForKey:key];
        [self storeData:data key:key];
    }
    [_cache setObject:data forKey:key];
}

- (void)storeObject:(id)object key:(NSString *)key
{
    [self storeData:[NSKeyedArchiver archivedDataWithRootObject:object] key:key];
}

- (void)storeData:(NSData *)data key:(NSString *)key completion:(void(^)(NSData *data, NSString *key))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self storeData:data key:key];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(data, key);
            }
        });
    });
}

- (void)storeObject:(id)object key:(NSString *)key completion:(void(^)(id object, NSString *key))completion
{
    [self storeData:[NSKeyedArchiver archivedDataWithRootObject:object] key:key completion:^(NSData *data, NSString *key) {
        if (completion) {
            completion(object, key);
        }
    }];
}

- (NSData*)dataForKey:(NSString *)key
{
    
    NSData* data = nil;
    if ([self isCached:key]) {
        data = [_cache objectForKey:key];
        if (!data) {
            data = [NSData dataWithContentsOfFile:[self pathForDataFile:key]];
        }
    }
    return data;
}

- (id)objectForKey:(NSString *)key
{
    NSData* data = nil;
    if ([self isCached:key]) {
        data = [_cache objectForKey:key];
        if (!data) {
            data = [NSData dataWithContentsOfFile:[self pathForDataFile:key]];
        }
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)dataForKey:(NSString *)key completion:(void(^)(NSData *data, NSString *key))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *dataFromDisk = [self dataForKey:key];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(dataFromDisk, key);
            }
        });
    });
}

- (void)objectForKey:(NSString *)key completion:(void(^)(id object, NSString *key))completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        id objectFromDisk = [NSKeyedUnarchiver unarchiveObjectWithData:[self dataForKey:key]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completion) {
                completion(objectFromDisk, key);
            }
        });
    });
}

- (void)removeCacheForKey:(NSString  *)key
{
    if ([self isCached:key]) {
        [[NSFileManager defaultManager] removeItemAtPath:[self pathForDataFile:key] error:nil];
    }
    [_cache removeObjectForKey:key];
}

- (void)removeAllCaches
{
    
    NSFileManager* fileManager=[NSFileManager defaultManager];
    NSString *cachePath = [self cachePath];
    if ([fileManager fileExistsAtPath:cachePath]) {
        [fileManager removeItemAtPath:cachePath error:nil];
    }
    
    [_cache removeAllObjects];
}

- (NSString*)internalKey:(NSString* )key
{
    const char *str = [key UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
}

@end