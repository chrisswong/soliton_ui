GTFoundation
===
GTFoundation includes all common functions / additions and views to start a project as quick as possible.

Foundation includes:

- Common Utils
- NSString, NSDate, NSArray, UIView, UILabel, UIScrollView, UIImage, UIColor category 
- Gradient view
- Language support
- Base64
- Hash (sha1, sha256, md5)
- Simplest http request helper
- Logger


###Requirements
- iOS 6.0 

###How to use

import via Cocoapods

```
pod 'GTFoundation', :git => 'git@gitlab.hk.gtomato.com:gt-team/gtfoundation-ios.git'
```

or import manually by copy all *.h and *.m files inside `GTFoundation` directory

###Notice

Most of the functions from categories has `gt_` as prefixer to prevent crashing with your existing category. Don't be afraid with it.

###Wish List

- [DONE] emptyStringIfNil
- [DONE] cornerRaidus (and complete circle view)
- collectionView reloadOneItemAtRow:Section:
- [DONE] UIAlertView With completion block (iOS 7 - 8 )
- [DONE] QR code reader ViewController using AVFoundcation in iOS 7 (Ref : <http://www.appcoda.com/qr-code-ios-programming-tutorial/>)
- [NOT CONSIDER TO ADD] NumberFormatter that similar to dateFormatter ( 1,234,567.890)
- [DONE] NSString case insenitive compare
```
	- (BOOL) isEqualIgnoreCaseWithString:(NSString *) string
	{
	    return [[self lowercaseString] isEqualToString:[string lowercaseString]];
	}
```
- List all available font in the app

```
// List available fonts in the app
for (NSString *name in [UIFont familyNames]) {
    NSLog(@"Family name : %@", name);
    for (NSString *font in [UIFont fontNamesForFamilyName:name]) {
        NSLog(@"Font name : %@", font);
    }
}
```  

- UITextView With placeholder string and placeholder textColor   



###Have problems?

Email to [wai.cheung@gtomato.com](wai.cheung@gtomato.com)