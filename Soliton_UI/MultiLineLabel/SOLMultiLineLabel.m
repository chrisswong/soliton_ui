//
//  SOLMultiLineLabel.m
//  Soliton_UI
//
//  Created by Chris Wong on 29/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import "SOLMultiLineLabel.h"

@implementation SOLMultiLineLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) awakeFromNib {
    [super awakeFromNib];
    //as it is multilined.
    self.numberOfLines = 0;
    self.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds);
    
}


@end
