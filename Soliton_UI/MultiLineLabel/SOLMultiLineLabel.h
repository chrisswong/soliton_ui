//
//  SOLMultiLineLabel.h
//  Soliton_UI
//
//  Created by Chris Wong on 29/5/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOLMultiLineLabel : UILabel

@end
