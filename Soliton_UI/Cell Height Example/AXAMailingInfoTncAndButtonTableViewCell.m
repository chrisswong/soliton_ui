//
//  AXAMailingInfoTncAndButtonTableViewCell.m
//  iRunForLove
//
//  Created by Chris on 24/12/14.
//  Copyright (c) 2014 Green Tomato. All rights reserved.
//

#import "AXAMailingInfoTncAndButtonTableViewCell.h"
#import "AXAGiftDetail.h"

NSString *AXAMailingInfoTncAndButtonTableViewCellIdentifier = @"AXAMailingInfoTncAndButtonTableViewCellIdentifier";

@interface AXAMailingInfoTncAndButtonTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *tncLabel;
@property (weak, nonatomic) IBOutlet UIButton *finishButton;


@end

@implementation AXAMailingInfoTncAndButtonTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self.finishButton setTitle:gt_localizedString(@"Common.Next") forState:UIControlStateNormal];
    UIImage *actionButtonBackgroundImage = [[UIImage imageNamed:@"btn_bg_blue"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
    [self.finishButton setBackgroundImage:actionButtonBackgroundImage forState:UIControlStateNormal];
    
    self.tncLabel.preferredMaxLayoutWidth = self.width - 8 * 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureWithGiftDetail:(AXAGiftDetail *) giftDetail {
    
    self.tncLabel.text = giftDetail.giftTermsAndCondition;
}

#pragma mark - Action

- (IBAction)finishButtonDidTap:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(mailingInfoTncAndButtonTableViewCellfinishButtonDidTapWithCell:)]) {
        [self.delegate mailingInfoTncAndButtonTableViewCellfinishButtonDidTapWithCell:self];
    }
}

+ (CGFloat) requiredHeightWithGiftDetail:(AXAGiftDetail *) giftDetail
 {
    AXAMailingInfoTncAndButtonTableViewCell *cell = [AXAMailingInfoTncAndButtonTableViewCell gt_viewFromNib];
     [cell configureWithGiftDetail:giftDetail];
     CGFloat height = [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
     return height;
}

@end
