//
//  AXAMailingInfoTncAndButtonTableViewCell.h
//  iRunForLove
//
//  Created by Chris on 24/12/14.
//  Copyright (c) 2014 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AXAGiftDetail;
@class AXAMailingInfoTncAndButtonTableViewCell;

extern NSString *AXAMailingInfoTncAndButtonTableViewCellIdentifier;

@protocol AXAMailingInfoTncAndButtonTableViewCellDelegate <NSObject>

- (void) mailingInfoTncAndButtonTableViewCellfinishButtonDidTapWithCell:(AXAMailingInfoTncAndButtonTableViewCell *)cell;

@end

@interface AXAMailingInfoTncAndButtonTableViewCell : UITableViewCell

@property (nonatomic, weak) id <AXAMailingInfoTncAndButtonTableViewCellDelegate> delegate;

- (void) configureWithGiftDetail:(AXAGiftDetail *) giftDetail;

+ (CGFloat) requiredHeightWithGiftDetail:(AXAGiftDetail *) giftDetail;

@end
