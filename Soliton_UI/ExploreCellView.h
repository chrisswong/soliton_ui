//
//  ExploreCellView.h
//  Soliton_UI
//
//  Created by Joyce Tam on 1/6/15.
//  Copyright (c) 2015 Green Tomato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExploreCellView : UIView

- (void)configCellWithImgUrl:(NSURL *)imgUrl title:(NSString *)title subTitle:(NSString *)subTitle;

@property (nonatomic, copy) void (^exploreCellViewPlayButtonDidTap)(UIButton *sender);

@end
